;;; config-local.el -*- lexical-binding: t; -*-
;; Place host specific configuration

(setq user-full-name "Gerry Agbobada"
      user-mail-address "jeff@amazon.com" ; Use the email address linked to the GPG Key on this host
      epa-file-encrypt-to user-mail-address)

;;; UI
;; Ignore errors if the fonts aren't found.
(ignore-errors
  (setq doom-font (font-spec :family "Iosevka Clapoto" :size 14 :weight 'semi-light)
        doom-big-font (font-spec :family "Iosevka Clapoto" :size 26)
        doom-variable-pitch-font (font-spec :family "Asap" :height 1.0)
        doom-serif-font (font-spec :family "Bitter" :height 1.0)))

(custom-set-faces!
 '(+workspace-tab-face :inherit default :family "Bitter" :height 135)
 '(+workspace-tab-selected-face :inherit (highlight +workspace-tab-face)))

(setq +pretty-code-fira-font-name "Fira Code Symbol"
      +pretty-code-hasklig-font-name "Hasklig"
      +pretty-code-iosevka-font-name "Iosevka")

(setq fancy-splash-image (concat doom-user-dir "banners/narf.png"))

;; For solar.el; using Paris as default
(setq calendar-latitude 48.856613
      calendar-longitude 2.352222)

;;;; Frames/Windows
;;;;; Maximize window
(add-hook 'window-setup-hook #'toggle-frame-maximized)
;;;;; Fringe
(setq-default fringe-mode 4)
(set-fringe-mode 4)
;;;;; Title
(setq frame-title-format
      '(""
        "%b"
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format " ● %s" project-name))))))

;;; Org
(setq org-directory (file-name-as-directory "~/path/to/org/"))
(setq org-roam-db-location (expand-file-name "~/.local/share/org-roam/org-roam.db"))
(setq org-plantuml-jar-path "~/soft/plantuml.jar")

;;; Denote
(setq denote-directory (file-name-as-directory "~/Documents/denote/"))
(setq consult-notes-sources `(("Notes" ?n ,denote-directory
                                ("Privé" ?p ,(expand-file-name "privé" denote-directory)))))

;;; Garbage Collection
(setq garbage-collection-messages nil)  ; For debugging
(setq gcmh-high-cons-threshold (* 1024 1024 3))

;;; LSP trickery
(setq read-process-output-max (* 1024 1024)) ; 1 MiB >> 4KB
(setq lsp-file-watch-threshold 50)
(setq +lua-lsp-dir (file-name-as-directory (expand-file-name "~/soft/lua-language-server/")))
(setq lsp-clients-lua-language-server-install-dir (file-name-as-directory (expand-file-name "~/soft/lua-language-server")))


;;; Email config
(after! mu4e
  (setq sendmail-program (executable-find "msmtp")
        send-mail-function #'smtpmail-send-it
        message-sendmail-f-is-evil t
        message-sendmail-extra-arguments '("--read-envelope-from")
        message-send-mail-function #'message-send-mail-with-sendmail))
(set-email-account! "foo.net"
  '((mu4e-sent-folder       . "/foo.net/Sent")
    (mu4e-drafts-folder     . "/foo.net/Drafts")
    (mu4e-trash-folder      . "/foo.net/Trash")
    (mu4e-refile-folder     . "/foo.net/Archive")
    (smtpmail-smtp-user     . "foo@foo.net")
    (mu4e-compose-signature . "---\nGerry Agbobada"))
  t)

