;;; ~/.config/doom/+local_config_example.el -*- lexical-binding: t; -*-
;; Place host specific configuration

(setq user-full-name "John Doe"
      user-mail-address "john@doe.test" ; Use the email address linked to the GPG Key on this host
      epa-file-encrypt-to user-mail-address
      conda-env-home-directory "/home/jdoe/.conda")


(setq calendar-latitude 48.85341
      calendar-longitude 2.3488)

;;; UI
;; Ignore errors if the fonts aren't found.
(ignore-errors
  (setq doom-font (font-spec :family "Source Code Pro" :size 14 :weight 'semi-light)
        doom-big-font (font-spec :family "Source Code Pro" :size 26)
        doom-variable-pitch-font (font-spec :family "Canberra" :height 1.0)
        doom-serif-font (font-spec :family "Source Code Pro" :height 1.0)))

(setq +pretty-code-fira-font-name "Fira Code Symbol"
      +pretty-code-hasklig-font-name "Hasklig"
      +pretty-code-iosevka-font-name "Iosevka")

(setq fancy-splash-image (concat doom-private-dir "banners/narf.png"))

(setq display-line-numbers-type nil)

;;;; Frames/Windows
;;;;; Maximize window
(add-hook 'window-setup-hook #'toggle-frame-maximized)
;;;;; Fringe
(setq-default fringe-mode 4)
(set-fringe-mode 4)
;;;;; Title
(setq frame-title-format
      '(""
        "%b"
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format " ● %s" project-name))))))

;;; Org
(setq org-directory (file-name-as-directory "~/path/to/org/"))
(setq org-plantuml-jar-path "~/path/to/plantuml.jar")

;;; Garbage Collection
(setq garbage-collection-messages nil)  ; For debugging
(setq gcmh-high-cons-threshold (* 1024 1024 3))

;;; LSP trickery
(setq read-process-output-max (* 1024 1024)) ; 1 MiB >> 4KB
(setq lsp-file-watch-threshold 50)
