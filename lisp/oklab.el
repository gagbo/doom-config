;;; lisp/oklab.el --- Utility functions for Björn Ottoson's OkLAB colorspace -*- coding: utf-8; lexical-binding: t; -*-
;;;
;;; Source: https://bottosson.github.io/posts/oklab/
;;; Version of the matrix: 2021.01.25
;;; Author: Gerry Agbobada <git@gagbo.net>

;;;; Helpers
(defun oklab--truncate (flt)
  "Truncate FLT, a (potentially double precision) float between 0.0 and 1.0,
to the maximum IEEE 754 single float precision.

To reach the maximum precision, the truncator is chosen to be 1/2^23, where 23 is the number
of bits reserved to the mantissa for 32-bit long IEEE754 floating point numbers"
  ;; The value of the truncator is chosen to please the binary representation Gods.
  (let ((truncator 0.00000011920928955078125))
    (* (truncate flt truncator) truncator)))

(defun oklab--gamma-transfer-function (lin_col)
  "Transfer color from linear space to perceptual sRGB space."
  ;; This function does not truncate the precision with oklab--truncate
  ;; because it is a private function and we want to avoid losing precision until the very end.
  (if (>= lin_col 0.0031308)
      (- (* 1.055 (expt lin_col (/ 2.4))) 0.055)
    (* lin_col 12.92)))

(defun oklab--inverse-gamma-transfer-function (per_col)
  "Transfer color from perceptual space to linear sRGB space."
  ;; This function does not truncate the precision with oklab--truncate
  ;; because it is a private function and we want to avoid losing precision until the very end.
  (if (>= per_col 0.04045)
      (expt (/ (+ per_col 0.055) 1.055) 2.4)
    (/ per_col 12.92)))

(defun oklab--srgb-to-linear (red green blue)
  "Convert sRGB to linear sRGB.

This function assumes the transfer function is a power function
(lambda (x) (math-pow x gamma)) with gamma = (/ 2.2)."
  (seq-map #'oklab--inverse-gamma-transfer-function (list red green blue)))

(defun oklab--linear-to-srgb (red green blue)
  "Convert linear sRGB to sRGB.

This function assumes the transfer function is a power function
(lambda (x) (math-pow x gamma)) with gamma = (/ 2.2)."
  (seq-map #'oklab--gamma-transfer-function (list red green blue)))


;;;; Public functions
(defun oklab-linear-srgb-to-lab (red green blue)
  "Convert linear sRGB to Ok L*a*b*."
  (let* ((l (+ (* red 0.4122214708) (* green 0.5363325363) (* blue 0.0514459929)))
         (m (+ (* red 0.2119034982) (* green 0.6806995451) (* blue 0.1073969566)))
         (s (+ (* red 0.0883024619) (* green 0.2817188376) (* blue 0.6299787005)))
         (l- (expt l (/ 3.0)))
         (m- (expt m (/ 3.0)))
         (s- (expt s (/ 3.0))))
    (list
     (oklab--truncate (+ (* l- 0.2104542553) (* m- 0.7936177850) (* s- (- 0.0040720468))))
     (oklab--truncate (+ (* l- 1.9779984951) (* m- (- 2.4285922050)) (* s- 0.4505937099)))
     (oklab--truncate (+ (* l- 0.0259040371) (* m- 0.7827717662) (* s- (- 0.8086757660)))))))

(defun oklab-lab-to-linear-srgb (L a b)
  "Convert Ok L*a*b* to RGB."
  (let* ((l- (+ L (* a 0.3963377774) (* b 0.2158037573)))
         (m- (+ L (* a (- 0.1055613458)) (* b (- 0.0638541728))))
         (s- (+ L (* a (- 0.0894841775)) (* b (- 1.2914855480))))
         (l (expt l- 3))
         (m (expt m- 3))
         (s (expt s- 3)))
    (list
     (oklab--truncate (+ (* l 4.0767416621) (* m (- 3.3077115913)) (* s 0.2309699292)))
     (oklab--truncate (+ (* l (- 1.2684380046)) (* m 2.6097574011) (* s (- 0.3413193965))))
     (oklab--truncate (+ (* l (- 0.0041960863)) (* m (- 0.7034186147)) (* s 1.7076147010))))))

(defun oklab-lab-to-lch (L a b)
  "Convert Ok L*a*b* to L*C*h*."
  (list L
        (sqrt (+ (* a a) (* b b)))
        (atan b a)))

(defun oklab-lch-to-lab (L C h)
  "Convert Ok L*C*h* to L*a*b*."
  (list L
        (* C (cos h))
        (* C (sin h))))

(defun oklab-srgb-to-lab (red green blue)
  "Convert RGB to Ok L*a*b*."
  (apply #'oklab-linear-srgb-to-lab (oklab--srgb-to-linear red green blue)))

(defun oklab-lab-to-srgb (L a b)
  "Convert Ok L*a*b* to RGB."
  (apply #'oklab--linear-to-srgb (oklab-lab-to-linear-srgb L a b)))

(defun oklab-srgb-to-lch (red green blue)
  "Convert RGB to Ok L*C*h*."
  (apply #'oklab-lab-to-lch (apply #'oklab-linear-srgb-to-lab (oklab--srgb-to-linear red green blue))))

(defun oklab-lch-to-srgb (L C h)
  "Convert Ok L*C*h* to RGB."
  (apply #'oklab--linear-to-srgb (apply #'oklab-lab-to-linear-srgb (oklab-lch-to-lab L C h))))

(defun oklab-change-l (red green blue l-change)
  "Change the lightness of a sRGB color."
  (let ((lch (oklab-srgb-to-lch red green blue)))
    (oklab-lch-to-srgb (+ (car lch) l-change) (cadr lch) (caddr lch))))

(defun oklab-change-c (red green blue c-change)
  "Change the chroma of a sRGB color."
  (let ((lch (oklab-srgb-to-lch red green blue)))
    (oklab-lch-to-srgb (car lch) (+ (cadr lch) c-change) (caddr lch))))

(defun oklab-change-h (red green blue h-change)
  "Change the hue of a sRGB color."
  (let ((lch (oklab-srgb-to-lch red green blue)))
    (oklab-lch-to-srgb (car lch) (cadr lch) (+ (caddr lch) h-change))))

(provide 'oklab)

;;; oklab.el ends here
