;;; config-org.el -*- lexical-binding: t; -*-
;; Org specific configuration
;; Note : Do not setq org-directory here, this is a host specific setting and
;; should go in config-local.el

(set-popup-rules!
  '(("^\\*Org.*Export\\*" :side right :size 0.5 :modeline t)))

(defconst gagbo/org-people-tag "personnes"
  "Tag utilisé pour marquer les nœuds roam qui concernent des vrais gens.")
(defconst gagbo/org-project-tag "projet"
  "Tag utilisé pour marquer les nœuds roam qui concernent des projets.")
(defvar gagbo/capture-inbox-file
  (expand-file-name (format "inbox-%s.org" (system-name)) org-directory)
  "The path to the inbox file.

  It is relative to `org-directory', unless it is absolute.")

(after! ox
  (require 'ox-confluence))

(use-package! ox-gfm
  :after org)

(use-package! org-modern
  :after org
  :config (global-org-modern-mode))

(after! org-roam
  (setq org-hugo-front-matter-format "yaml"
        ;; Roam is setup as a sibling of the main org directory
        org-roam-directory (file-name-as-directory (expand-file-name (concat (file-name-as-directory org-directory) "../org-roam/")))
        org-roam-dailies-directory (file-name-as-directory "journaux")
        org-roam-dailies-capture-templates
        '(("d" "default" entry
           "* %?"
           :target (file+head "%<%Y-%m-%d>.org"
                              "#+title: %<%Y-%m-%d>\n")))
        org-roam-capture-templates
        '(("dc" "domaine / Claviers Souris" plain
           "%?"
           :target (file+head "domaines/claviers_souris/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("de" "domaine / Emploi courant" plain
           "%?"
           :target (file+head "domaines/emploi/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("dm" "domaine / Maison" plain
           "%?"
           :target (file+head "domaines/maison/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("do" "domaine / Organisation" plain
           "%?"
           :target (file+head "domaines/organisation/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("dt" "domaine / Ou[T]ils informatique" plain
           "%?"
           :target (file+head "domaines/outils_informatique/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("dp" "domaine / Programmation" plain
           "%?"
           :target (file+head "domaines/programmation/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("dr" "domaine / Relations" plain
           "%?"
           :target (file+head "domaines/relations/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t)
          ("ds" "domaine / Santé" plain
           "%?"
           :target (file+head "domaines/santé/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t))

        ;; Attachment is setup as a sibling of the main org directory
        org-attach-id-dir (file-name-as-directory (concat (file-name-as-directory org-directory) "data/"))))

(after! org
  (setq
   org-reveal-theme "solarized"

   org-log-done 'time
   org-log-into-drawer t

   org-time-stamp-rounding-minutes '(1 1)

   org-link-mailto-program '(compose-mail "%a" "%s")

   ;; Require braces to make org consider_{this} or^{this} as subscript or superscript
   org-use-sub-superscripts "{}"

   org-tags-exclude-from-inheritance '("crypt")
   ;; Default value that I want enforced no matter what.
   org-use-property-inheritance nil)

;;; Workflow states
  (setq
   ;; This setting sets the different workflow states. For the bug
   ;; reports, each project should have its own #+SEQ_TODO property
   org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "WAIT(w@/!)"
      "|"
      "DONE(d)" "CANCELLED(c@/!)"
      "PHONE" "MEETING")
     (sequence "PROJ" "|" "DONE"))

   ;; The triggers break down to the following rules:

   ;; - Moving a task to =CANCELLED= adds a =CANCELLED= tag
   ;; - Moving a task to =WAIT= adds a =WAIT= tag
   ;; - Moving a task to a done state removes =WAIT= and =HOLD= tags
   ;; - Moving a task to =TODO= removes =WAIT=, =CANCELLED=, and =HOLD= tags
   ;; - Moving a task to =NEXT= removes =WAIT=, =CANCELLED=, and =HOLD= tags
   ;; - Moving a task to =DONE= removes =WAIT=, =CANCELLED=, and =HOLD= tags
   org-todo-state-tags-triggers
   '(("CANCELLED" ("cancelled" . t))
     ("WAIT" ("waiting" . t))
     (done ("waiting") ("hold"))
     ("TODO" ("waiting") ("cancelled") ("hold"))
     ("NEXT" ("waiting") ("cancelled") ("hold"))
     ("DONE" ("waiting") ("cancelled") ("hold")))

   ;; This settings allows to fixup the state of a todo item without
   ;; triggering notes or log.
   org-treat-S-cursor-todo-selection-as-state-change nil)

;;; Agenda
  (setq
   org-archive-location (concat (file-name-as-directory org-directory) "archive.org::* Depuis %s")
   org-agenda-files (list (expand-file-name "next.org" org-directory)
                          (expand-file-name "projects.org" org-directory)
                          (expand-file-name "tickler.org" org-directory)
                          (expand-file-name "inbox.org" org-directory)
                          (expand-file-name "inbox-suicune.org" org-directory)
                          (expand-file-name "inbox-szl-gag.org" org-directory)
                          (expand-file-name "inbox-aligatueur.org" org-directory)
                          (expand-file-name "inbox-mobile.org" org-directory))

   org-agenda-prefix-format
   '((agenda . " %i %(gagbo/org-agenda-category 18)%?-18t% s")
     (todo   . " %i %(gagbo/org-agenda-category 18) ")
     (tags   . " %i %(gagbo/org-agenda-category 18) ")
     (search . " %i %(gagbo/org-agenda-category 18) "))
   ;; Warn about deadlines 30 days prior
   ;; To change it for a heavily recurring task, use
   ;;   DEADLINE: <2009-07-01 Wed +1m -0d>
   ;; + => recurrence
   ;; - => deadline warning
   org-deadline-warning-days 30

   ;; Dim blocked tasks
   org-agenda-dim-blocked-tasks t
   org-agenda-compact-blocks nil
   org-agenda-span 'week

   ;; Sorting order for tasks on the agenda
   org-agenda-sorting-strategy
   '((agenda habit-down time-up user-defined-up effort-up category-keep)
     (todo priority-down effort-up category-up)
     (tags priority-down effort-up category-up)
     (search category-keep))

   ;; Start the weekly agenda on Monday
   org-agenda-start-on-weekday 1
   org-agenda-start-day "+0d"

   ;; Enable display of the time grid so we can see the marker for the current time
   org-agenda-time-grid '((daily today require-timed)
                          (0800 1000 1200 1400 1600 1800 2000)
                          "......" "----------------")

   ;; Display tags farther right
   org-agenda-tags-column 'auto

   ;; Use sticky agenda's so they persist
   org-agenda-sticky t)


;;;; Agenda commands
  (setq org-agenda-custom-commands
        `((" " "Refile"
           ((tags
             "REFILE"
             ((org-agenda-overriding-header "To refile")))))

          ("g" . "Getting Things Done (GTD)")
          ("gn" "Idées à creuser"
           ((tags "+incubate/PROJ"
                  ((org-agenda-files (list ,(expand-file-name "incubate.org" org-directory)))
                   (org-agenda-overriding-header "Idées à creuser")))))
          ("gb" "Boulot"
           ((agenda ""
                    ((org-agenda-skip-function
                      '(org-agenda-skip-entry-if 'deadline))
                     (org-deadline-warning-days 0)))
            (agenda ""
                    ((org-agenda-entry-types '(:deadline))
                     (org-agenda-format-date "")
                     (org-deadline-warning-days 14)
                     (org-agenda-span 'day)
                     (org-agenda-overriding-header "Deadlines")))
            (todo "NEXT"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline))
                   (org-agenda-overriding-header "Tâches")))
            (todo "WAIT"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline))
                   (org-agenda-overriding-header "En attente")))
            (stuck ""
                   ((org-stuck-projects '("/PROJ" ("NEXT" "WAIT") nil nil))
                    (org-agenda-overriding-header "Projets Bloqués")))
            (tags "CLOSED>=\"<today>\""
                  ((org-agenda-overriding-header "Completed today"))))
           ;; This sets the whole command to filter everything and keep only :@boulot: items
           ((org-agenda-tag-filter-preset '("+@boulot-incubate"))))

          ("gv" "Vie privée"
           ((agenda ""
                    ((org-agenda-skip-function
                      '(org-agenda-skip-entry-if 'deadline))
                     (org-deadline-warning-days 0)))
            (agenda ""
                    ((org-agenda-entry-types '(:deadline))
                     (org-agenda-format-date "")
                     (org-deadline-warning-days 14)
                     (org-agenda-span 'day)
                     (org-agenda-overriding-header "Deadlines")))
            (todo "NEXT"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline))
                   (org-agenda-overriding-header "Tâches")))
            (todo "WAIT"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline))
                   (org-agenda-overriding-header "En attente")))
            (stuck ""
                   ((org-stuck-projects '("/PROJ" ("NEXT" "WAIT") nil nil))
                    (org-agenda-overriding-header "Projets Bloqués")))
            (tags "CLOSED>=\"<today>\""
                  ((org-agenda-overriding-header "Completed today"))))
           ;; This sets the whole command to filter everything and keep only _non_-:@boulot: items
           ((org-agenda-tag-filter-preset '("-@boulot-incubate"))))))

;;; Stuck project detection
  (setq org-stuck-projects
        '("/PROJ" ("NEXT" "WAIT") nil nil))

;;; Capture templates
  (setq org-capture-templates
        `(("t" "todo" plain (file gagbo/capture-inbox-file)
           "* TODO %?\n%U\n" :clock-in t :clock-resume t)))

;;; Refile targets
  (setq org-refile-allow-creating-parent-nodes t
        org-refile-use-outline-path t
        org-refile-targets `((,(expand-file-name "incubate.org" org-directory) :level 2)
                             (,(expand-file-name "next.org" org-directory) :level 1)
                             (,(expand-file-name "projects.org" org-directory) :level 1))
        org-outline-path-complete-in-steps nil)

;;; Clocking
  ;; Resume clocking task when emacs is restarted
  (org-clock-persistence-insinuate)
  (setq
   ;; Show lot of clocking history so it's easy to pick items off the C-F11 list
   org-clock-history-length 23
   ;; Resume clocking task on clock-in if the clock is open
   org-clock-in-resume t
   ;; Separate drawers for clocking and logs
   org-drawers (quote ("PROPERTIES" "LOGBOOK"))
   ;; Save clock data and state changes and notes in the LOGBOOK drawer
   org-clock-into-drawer t
   ;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
   org-clock-out-remove-zero-time-clocks t
   ;; Cl!ock out when moving task to a done state
   org-clock-out-when-done t
   ;; Save the running clock and all clock history when exiting Emacs, load it on startup
   org-clock-persist t
   ;; Enable auto clock resolution for finding open clocks
   org-clock-auto-clock-resolution 'when-no-clock-is-running
   ;; Include current clocking task in clock reports
   org-clock-report-include-clocking-task t

   org-agenda-clock-consistency-checks
   '(:max-duration "4:00"
     :min-duration 0
     :max-gap 0
     :gap-ok-around ("4:00"))

   ;; Agenda clock report parameters
   org-agenda-clockreport-parameter-plist
   '(:link t :maxlevel 5 :fileskip0 t :compact t :narrow 80)

   ;; Set default column view headings: Task Effort Clock_Summary
   org-columns-default-format "%80ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM"

   ;; global Effort estimate values
   ;; global STYLE property values for completion
   org-global-properties
   '(("Effort_ALL" . "0:15 0:30 1:00 2:00 5:00 10:00")
     ("STYLE_ALL" . "habit"))

   ;; Agenda log mode items to display (closed and state changes by default)
   org-agenda-log-mode-items
   '(closed state))

;;; Export and Publishing
  (setq
   org-latex-src-block-backend 'listings
   org-table-export-default-format "orgtbl-to-csv"))
