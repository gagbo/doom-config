;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

(defmacro current-no-native-compile-list! (&rest features)
  "Mark the list of FEATURES as non-native compilable"
  `(with-eval-after-load 'comp
     (mapc (apply-partially #'add-to-list 'comp-deferred-compilation-deny-list)
           (let ((local-dir-re (concat "\\`" (regexp-quote doom-local-dir))))
             (list ,@(mapcar
                      (lambda (feat) `(concat local-dir-re ".*/" ,feat "\\.el\\'"))
                      features))))))

(defvar comp-deferred-compilation-deny-list '())


;;; Unpin packages
(unpin! t)

;;; Tools
(package! imenu-list)
(package! jinja2-mode)
(package! realgud-lldb)
(package! key-quiz)
(package! fish-completion)
(package! outshine)
(package! evil-escape :disable t)
(package! just-ts-mode)

(package! vundo :recipe (:host github :repo "casouri/vundo"))

;;; Themes
(package! modus-themes)
(package! ef-themes :recipe (:host nil :repo "https://git.sr.ht/~protesilaos/ef-themes"))
(package! solaire-mode :disable t)
(package! circadian)
(package! kurecolor)
(package! ct :recipe (:host github :repo "neeasade/ct.el" :branch "master"))
(package! doom-alabaster-theme :recipe (:host github :repo "agraul/doom-alabaster-theme"))

;;; UI
(package! page-break-lines)
(package! info-colors)

;;; Org
(package! ox-gfm)
(package! org-modern)

;;; Languages
;;;; Golang
(when (and (modulep! :checkers syntax)
           (modulep! :lang go)
           (not (modulep! :checkers syntax +flymake)))
  (package! flycheck-golangci-lint))
;;;; Python
(package! blacken)
(package! anaconda-mode :disable t)
(package! nose :disable t)
;;;; Plantuml
(package! plantuml-mode)
;;;; Elisp
(when (and (modulep! :checkers syntax)
           (not (modulep! :checkers syntax +flymake)))
  (package! flycheck-package))
;;;; Rust (Pest mode)
(package! pest-mode)
;;;; CapnProto
(package! capnp-mode)


;; Local packages
(package! consult-lsp
  :recipe (:local-repo "lisp/consult-lsp"
           :build (:not compile)))
(package! rhai-mode
  :recipe (:local-repo "lisp/rhai-mode"
           :build (:not compile)))
(package! clapoto-themes
  :recipe (:local-repo "lisp/emacs-clapoto-themes"
           :build (:not compile)))
(package! lsp-ai
  :recipe (:local-repo "lisp/lsp-ai"
           :build (:not compile)))

;; Forks
;; (package! parinfer-rust :disable t)
(package! multi-vterm
  :recipe (:local-repo "lisp/multi-vterm"
           :build (:not compile)))
