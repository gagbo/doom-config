;;; ~/.doom.d/config.el --- Private Doom Emacs config -*- lexical-binding: t; -*-


;;; Set up load path and host specific configuration
(let ((default-directory (expand-file-name "lisp" doom-user-dir)))
  (normal-top-level-add-subdirs-to-load-path))
(setq load-path (append
                 (mapcar (lambda (folder) (expand-file-name folder doom-user-dir))
                         '("lisp"))
                 load-path))
(load! "config-local")


;;; Theme
(setq doom-theme 'ef-bio
      doom-themes-treemacs-theme 'doom-colors
      doom-acario-dark-brighter-modeline t
      doom-acario-light-brighter-modeline t
      doom-natnat-brighter-modeline t
      doom-rose-pine-dawn-brighter-modeline nil
      doom-nohl-light-brighter-comments t
      doom-nohl-dark-brighter-comments t
      doom-natnat-brighter-comments t
      doom-rose-pine-dawn-brighter-comments t
      doom-everforest-light-brighter-comments t
      doom-everforest-dark-brighter-comments t)

(setq display-line-numbers-type 'relative)
;; (setq telephone-line-target 'header-line)
;; (setq-default mode-line-format nil)

;; From hylo: https://discourse.doomemacs.org/t/permanently-display-workspaces-in-the-tab-bar/4088
(when (modulep! :ui workspaces)
  (after! persp-mode
    (defun workspaces-formatted ()
      (+doom-dashboard--center (frame-width)
                               (let ((names (or persp-names-cache nil))
                                     (current-name (safe-persp-name (get-current-persp))))
                                 (mapconcat
                                  #'identity
                                  (cl-loop for name in names
                                           for i to (length names)
                                           collect
                                           (concat (propertize (format " %d" (1+ i)) 'face
                                                               `(:inherit ,(if (equal current-name name)
                                                                               '+workspace-tab-selected-face
                                                                             '+workspace-tab-face)
                                                                 :weight bold))
                                                   (propertize (format " %s " name) 'face
                                                               (if (equal current-name name)
                                                                   '+workspace-tab-selected-face
                                                                 '+workspace-tab-face))))
                                  " "))))

    (defun gagbo/invisible-current-workspace ()
      "The tab bar doesn't update when only faces change (i.e. the
     current workspace), so we invisibly print the current workspace
     name as well to trigger updates"
      (propertize (safe-persp-name (get-current-persp)) 'invisible t))

    (customize-set-variable 'tab-bar-format '(workspaces-formatted tab-bar-format-align-right gagbo/invisible-current-workspace))

    ;; don't show current workspaces when we switch, since we always see them
    (advice-add #'+workspace/display :override #'ignore)
    ;; same for renaming and deleting (and saving, but oh well)
    (advice-add #'+workspace-message :override #'ignore)

    ;; need to run this later for it to not break frame size for some reason
    (run-at-time nil nil (cmd! (tab-bar-mode +1)))))


;;;; Extra theme customizations
(use-package! ef-themes
  :init
  (setq enable-theme-functions
        (cons (lambda (current-theme)
                (let* ((theme-name (symbol-name current-theme)))
                  (when (string-prefix-p "ef-" theme-name)
                    (gagbo/ef-themes-todo-faces)
                    (gagbo/ef-themes-custom-faces current-theme))))
              enable-theme-functions)))

(use-package! modus-themes
  :init
  (setq enable-theme-functions
        (cons (lambda (current-theme)
                (let* ((theme-name (symbol-name current-theme)))
                  (when (string-prefix-p "modus-" theme-name)
                    (gagbo/modus-themes-custom-faces current-theme))))
              enable-theme-functions))

  (setq modus-themes-headings
        '((1 . (variable-pitch 2))
          (2 . (1.5))
          (agenda-date . (1.5))
          (agenda-structure . (variable-pitch light 2))
          (t . (1.3)))
        modus-themes-org-blocks 'tinted-background
        modus-themes-bold-constructs t
        modus-themes-italic-constructs t
        modus-themes-prompts '(bold)
        modus-themes-completions '((matches . (extrabold))
                                   (selection . (semibold underline)))
        modus-themes-common-palette-overrides
        '(;; "Alt" syntax coloration
          (rx-construct magenta-warmer)
          (rx-backslash blue-cooler)

          ;; Reduce highlights
          (builtin fg-main)
          (comment green-faint)
          (constant fg-main)
          (fnname fg-main)
          (keyword fg-main)
          (preprocessor fg-main)
          (docstring green-cooler)
          (docmarkup magenta-faint)
          (string blue)
          (type fg-main)
          (variable fg-main)

          (underline-err red-faint)
          (underline-warning yellow-faint)
          (underline-note cyan-faint)

          (bg-region bg-sage) ; try to replace `bg-ochre' with `bg-lavender', `bg-sage'
          (fg-region unspecified)

          ;; Line numbers
          (fg-line-number-inactive "gray50")
          (fg-line-number-active fg-main)
          (bg-line-number-inactive unspecified)
          (bg-line-number-active unspecified)

          ;; Subtle links
          (underline-link border)
          (underline-link-visited border)
          (underline-link-symbolic border)

          ;; Completion palette
          (fg-completion-match-0 fg-main)
          (fg-completion-match-1 fg-main)
          (fg-completion-match-2 fg-main)
          (fg-completion-match-3 fg-main)
          (bg-completion-match-0 bg-blue-subtle)
          (bg-completion-match-1 bg-yellow-subtle)
          (bg-completion-match-2 bg-cyan-subtle)
          (bg-completion-match-3 bg-red-subtle)

          ;; Parens
          (bg-paren-match bg-blue-intense)
          (underline-paren-match fg-main)

          ;; Agenda
          (date-common cyan-faint)      ; for timestamps and more
          (date-deadline red-faint)
          (date-event fg-alt)           ; default
          (date-holiday magenta)        ; default (for M-x calendar)
          (date-now fg-main)            ; default
          (date-scheduled yellow-faint)
          (date-weekday fg-dim)
          (date-weekend fg-dim)

          (bg-hover bg-green-subtle))))


;;;; Org and Treemacs
;; (doom-themes-treemacs-config)
(doom-themes-org-config)

(use-package! circadian
  :init
  (setq circadian-themes '((:sunrise . doom-nord)
                           (:sunset  . doom-sourcerer)))
  (add-transient-hook! 'doom-init-ui-hook (circadian-setup)))


;;; Misc
;;;; undo
(map! :leader :desc "Undo tree" :n "U" #'vundo)
(after! vundo
  (setq vundo-glyph-alist vundo-unicode-symbols
        vundo-compact-display nil))

;;;; Window splitting
(setq evil-vsplit-window-right t
      evil-split-window-below t)

;;;; Global substitute
(setq evil-ex-substitute-global t)

;;;; Cursor past EOL
(setq evil-move-beyond-eol t)

;;;; Remove iedit message
(setq iedit-toggle-key-default nil)

;;;; Uniquify
(setq uniquify-buffer-name-style 'forward
      uniquify-strip-common-suffix t)

;;;; Which key
(setq which-key-idle-delay 0.3)

;;;; Tabs and final EOL
(setq-default tab-width 8)
(setq require-final-newline t)

;;;; Local variables
(setq-default enable-local-variables t)

;;;; Ophints
(setq evil-goggles-duration 0.2)

;;;; Magit
(after! magit
  (setq magit-revision-show-gravatars '("^Author:     " . "^Commit:     ")))

;;;; LSP
(when (or (modulep! :checkers syntax +flymake)
          (not (modulep! :checkers syntax)))
  (setq lsp-diagnostics-provider :flymake))
(after! lsp-mode
  (setq
   lsp-log-io nil
   lsp-auto-guess-root t
   lsp-progress-via-spinner t
   lsp-enable-file-watchers nil
   lsp-idle-delay 0.01
   lsp-completion-enable-additional-text-edit t

   lsp-signature-render-documentation t
   lsp-signature-auto-activate '(:on-trigger-char :on-server-request :after-completion)
   lsp-signature-doc-lines 10

   lsp-eldoc-enable-hover t
   lsp-headerline-breadcrumb-enable t
   lsp-modeline-code-actions-segments '(count icon name)

   lsp-enable-indentation nil
   lsp-enable-on-type-formatting nil
   lsp-enable-symbol-highlighting nil
   lsp-enable-links nil

   lsp-lens-enable t))

(after! lsp-ui
  (setq
   ;; Sideline
   lsp-ui-sideline-enable t
   lsp-ui-sideline-show-code-actions t
   lsp-ui-sideline-show-symbol nil
   lsp-ui-sideline-show-hover nil
   lsp-ui-sideline-show-diagnostics nil
   ;; Peek
   lsp-ui-peek-enable t
   ;; Doc
   lsp-ui-doc-enable t
   lsp-ui-doc-position 'top
   lsp-ui-doc-delay 0.51
   lsp-ui-doc-max-width 50
   lsp-ui-doc-max-height 30
   lsp-ui-doc-include-signature t
   lsp-ui-doc-show-with-cursor nil
   lsp-ui-doc-show-with-mouse nil
   lsp-ui-doc-header t))

;;;; Corfu
(after! corfu
  (setq! corfu-preselect 'prompt
         corfu-preview-current 'insert
         corfu-quit-no-match 'separator
         global-corfu-minibuffer nil
         corfu-popupinfo-delay '(0.5 . 0.5)
         corfu-auto-delay 0.2
         corfu-auto-prefix 2)
  (corfu-popupinfo-mode t))

(use-package completion-preview
  :bind (:map completion-preview-active-mode-map
              ("M-f" . #'completion-preview-insert-word)
              ("C-M-f" . #'completion-preview-insert-sexp)
              ("M-i" . #'completion-preview-insert)
              ("M-n" . #'completion-preview-next-candidate)
              ("M-p" . #'completion-preview-prev-candidate))
  :custom
  (completion-preview-minimum-symbol-length 2)
  :config
  ;; Non-standard commands to that should show the preview:

  ;; Org mode has a custom `self-insert-command'
  (push 'org-self-insert-command completion-preview-commands)
  ;; Paredit has a custom `delete-backward-char' command
  (push 'paredit-backward-delete completion-preview-commands)
  :init
  (global-completion-preview-mode))


;;;; Flycheck
(after! flycheck
  (setq flycheck-display-errors-delay 0.1))

;;;; Avy
(after! avy
  (setq avy-keys '(?n ?r ?p ?m ?e ?t ?o ?d ?h ?v ?i)
        lispy-avy-keys '(?n ?r ?p ?m ?e ?t ?o ?d ?h ?v ?i))
  (add-to-list 'avy-orders-alist '(avy-goto-word-0 . avy-order-closest))
  (add-to-list 'avy-orders-alist '(avy-goto-char . avy-order-closest))
  (add-to-list 'avy-orders-alist '(avy-goto-char-in-line . avy-order-closest))
  (add-to-list 'avy-orders-alist '(avy-goto-line . avy-order-closest))
  (add-to-list 'avy-orders-alist '(avy-goto-line-above . avy-order-closest))
  (add-to-list 'avy-orders-alist '(avy-goto-line-below . avy-order-closest)))

;;;; Consult
(after! consult
  (setq consult-narrow-key "C-,"
        consult-preview-key (list "C-<right>" "C-<left>")))


;;; Non-Doom packages

;;;; Rainbow mode
;; Fontify html hex codes
(use-package rainbow-mode
  :diminish
  :commands rainbow-mode)

;;;; Rainbow mode with overlays
(use-package ov-rainbow-mode
  :commands ov-rainbow-mode)

;;;; Key Quiz
(use-package! key-quiz
  :commands (key-quiz)
  :init
  (setq key-quiz-matching-regexp "^<?[MCSgz]"))

;;;; Page Break Lines
(use-package! page-break-lines
  :init
  (setq page-break-lines-modes '(emacs-lisp-mode lisp-mode scheme-mode compilation-mode outline-mode help-mode))
  :commands (global-page-break-lines-mode page-break-lines-mode))

(global-page-break-lines-mode)

;;;; Flymake
(after! flymake
  (setq flymake-fringe-indicator-position 'right-fringe
        flymake-suppress-zero-counters t
        flymake-start-on-flymake-mode t
        flymake-no-changes-timeout nil
        flymake-start-on-save-buffer t
        flymake-proc-compilation-prevents-syntax-check t
        flymake-wrap-around nil))

;;;; Fish
(when (and (executable-find "fish")
           (require 'fish-completion nil t))
  (global-fish-completion-mode))

;;;; Info colors
(use-package! info-colors
  :hook (Info-selection . info-colors-fontify-node))

;;;; Ledger custom
(setq! ledger-binary-path "hledger")
(defun hledger-rules-mode-setup ()
  (font-lock-add-keywords nil 
                          '(;; Comments
                            ("^\\s-*#.*" . font-lock-comment-face)

                            ;; Keywords
                            ("\\<\\(if\\|fields\\|date-format\\|currency\\|account[0-9]+\\|amount[0-9]+\\|skip\\)\\>" . font-lock-keyword-face)

                            ;; Date formats, accounts, etc.
                            ("\\<\\(%Y-%m-%d\\|\\b[a-zA-Z0-9:_-]+\\b\\)\\>" . font-lock-variable-name-face)

                            ;; Operators
                            ("\\(=~\\|==\\|!=\\|<=\\|>=\\)" . font-lock-builtin-face)

                            ;; Variables like %description, %amount
                            ("\\(%[a-zA-Z_]+\\)" . font-lock-variable-name-face)

                            ;; General words
                            ("\\b\\w+\\b" . font-lock-string-face))))

(define-minor-mode hledger-rules-mode
  "Minor mode for editing .rules files in hledger."
  :lighter " hledger-rules"
  (hledger-rules-mode-setup))

(add-to-list 'auto-mode-alist '("\\.rules\\'" . hledger-rules-mode))

;;;; Jinja2
(use-package! jinja2-mode
  :mode (("\\.j2\\'" . jinja2-mode)
         ("\\.jinja\\'" . jinja2-mode)))

;;;; elisp
(use-package! flycheck-package
  :after flycheck
  :config
  (flycheck-package-setup))

;;;; golang
(use-package! flycheck-golangci-lint
  :hook (go-mode . flycheck-golangci-lint-setup)
  :config (setenv "GO111MODULE" "on"))

(after! lsp
  (lsp-register-custom-settings
   '(("golangci-lint.command"
      ["golangci-lint" "run" "--enable-all" "--disable" "lll" "--out-format" "json" "--issues-exit-code=1"])))

  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection
                                     '("golangci-lint-langserver"))
                    :major-modes '(go-mode)
                    :language-id "go"
                    :priority 0
                    :server-id 'golangci-lint
                    :add-on? t
                    :library-folders-fn #'lsp-go--library-default-directories
                    :initialization-options (lambda ()
                                              (gethash "golangci-lint"
                                                       (lsp-configuration-section "golangci-lint"))))))
(after! lsp-go
  (setq lsp-go-use-gofumpt t))

;;;; org
(load! "config-org")

;;;; python
(after! lsp-pyls
  (setq lsp-pyls-plugins-pycodestyle-enabled nil ;; Disable to ensure sanity
        lsp-pyls-plugins-pylint-enabled nil ;; Disable to ensure performance
        lsp-pyls-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pyls-configuration-sources ["flake8"]))
(after! lsp-pylsp
  (setq lsp-pylsp-plugins-pycodestyle-enabled nil ;; Disable to ensure sanity
        lsp-pylsp-plugins-pylint-enabled nil ;; Disable to ensure performance
        lsp-pylsp-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pylsp-configuration-sources ["flake8"]))

(when (and (modulep! :checkers syntax)
           (not (modulep! :checkers syntax +flymake)))
  (after! (python flycheck)
    (gagbo--python-flycheck-setup)))

;;;; Javascript / Typescript
(after! lsp-eslint
  ;; There’s almost always prettier setup for formatting
  (setq lsp-eslint-format nil))


;;;;; Nix
(after! lsp-nix
  (setq lsp-nix-nixd-formatting-command ["alejandra"]
        lsp-nix-nixd-nixpkgs-expr "import <nixpkgs> { }"))


;;;; rust
(after! rustic
  (set-formatter! 'rustic-mode #'rustic-cargo-fmt))

(map! (:map rustic-mode-map
       :localleader
       :desc "Toggle LSP hints" "h" #'lsp-rust-analyzer-inlay-hints-mode))

(setq rustic-lsp-server 'rust-analyzer
      rustic-format-on-save t
      lsp-rust-server 'rust-analyzer)

(set-popup-rule!
  "^\\*rust"
  :slot -2
  :size 0.45
  :side 'right
  :autosave t
  :quit 'current
  :ttl nil
  :modeline t)

(custom-set-faces!
  `(lsp-rust-analyzer-inlay-face :inherit lsp-details-face))

(setq dap-cpptools-extension-version "1.18.0")

(after! lsp-rust
  (setq lsp-rust-analyzer-lru-capacity 100
        lsp-rust-analyzer-server-display-inlay-hints t
        lsp-rust-analyzer-display-chaining-hints t
        lsp-rust-analyzer-display-reborrow-hints t
        lsp-rust-analyzer-display-closure-return-type-hints t
        lsp-rust-analyzer-display-parameter-hints t
        lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial"
        lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names t
        lsp-rust-analyzer-cargo-watch-enable t
        lsp-rust-analyzer-cargo-run-build-scripts t
        lsp-rust-analyzer-proc-macro-enable t
        lsp-rust-analyzer-cargo-watch-command "clippy")

  ;; TODO: upstream those
  ;; do not cache the shitty result from rust-analyzer
  (advice-add #'lsp-eldoc-function :after (lambda (&rest _) (setq lsp--hover-saved-bounds nil)))
  ;; extract and show short signature for rust-analyzer
  (cl-defmethod lsp-clients-extract-signature-on-hover (contents (_server-id (eql rust-analyzer)))
    (let* ((value (if lsp-use-plists (plist-get contents :value) (gethash "value" contents)))
           (groups (--partition-by (s-blank? it) (s-lines (s-trim value))))
           (sig_group (if (s-equals? "```rust" (car (-third-item groups)))
                          (-third-item groups)
                        (car groups)))
           (sig (--> sig_group
                     (--drop-while (s-equals? "```rust" it) it)
                     (--take-while (not (s-equals? "```" it)) it)
                     (--map (s-trim it) it)
                     (s-join " " it))))
      (lsp--render-element (concat "```rust\n" sig "\n```"))))

  (require 'dap-cpptools))

(after! dap-cpptools
  (dap-register-debug-template "Rust::CppTools Run Configuration"
                               (list :type "cppdbg"
                                     :request "launch"
                                     :name "Rust::Run"
                                     :MIMode "gdb"
                                     :miDebuggerPath "rust-gdb"
                                     :environment []
                                     :program "${workspaceFolder}/target/debug/hello / replace with binary"
                                     :cwd "${workspaceFolder}"
                                     :console "external"
                                     :dap-compilation "cargo build"
                                     :dap-compilation-dir "${workspaceFolder}")))

(after! dap-mode
  (setq dap-default-terminal-kind "integrated")
  (dap-auto-configure-mode +1))

;;;; imenu-list
(use-package! imenu-list
  :commands imenu-list-smart-toggle)

;;;; Treemacs
(after! treemacs
  (set-popup-rule! "^ \\*Treemacs"
    :side 'left
    :slot 1
    :size 0.30
    :quit nil
    :ttl 0)
  (set-popup-rule! "^\\*LSP Symbols List\\*$"
    :side 'left
    :slot 2
    :size 0.30
    :quit nil
    :ttl 0))

;;;; Debugging
;; GDB settings
(setq gdb-show-main t
      gdb-many-windows t)

(use-package! realgud-lldb
  :defer t
  :init (add-to-list '+debugger--realgud-alist
                     '(realgud:lldb :modes (c-mode c++-mode rust-mode)
                       :package realgud-lldb)))

;;; Faces
(custom-set-faces!
  `(corfu-annotations
    :slant oblique)
  '(tree-sitter-hl-face:property :slant oblique))


;;; Miscellaneous popup rules

(set-popup-rules!
  '(("^\\*info\\*"
     :slot 2 :side left :width 83 :quit nil)
    ("^\\*\\(?:Wo\\)?Man "
     :vslot -6 :size 0.45 :select t :quit nil :ttl 0)
    ("^\\*ielm\\*$"
     :vslot 2 :size 0.4 :quit nil :ttl nil)
    ("^\\*Ilist\\*$"
     :slot 2 :side left :size 0.3 :quit nil :ttl nil)
    ;; `help-mode', `helpful-mode'
    ("^\\*[Hh]elp"
     :slot 2 :vslot -8 :size 0.45 :select t)
    ("^\\*Checkdoc Status\\*$"
     :vslot -2 :select ignore :quit t :ttl 0)
    ("^\\*\\(?:[Cc]ompil\\(?:ation\\|e-Log\\)\\|Messages\\)"
     :slot -2 :size 0.45 :side right :autosave t :quit current :ttl nil
     :modeline t)
    ("^ \\*\\(?:undo-tree\\|vundo tree\\)\\*"
     :slot 2 :side left :size 20 :select t :quit t)
    ("^\\*\\(?:doom \\|Pp E\\)"  ; transient buffers (no interaction required)
     :vslot -3 :size +popup-shrink-to-fit :autosave t :select ignore :quit t :ttl 0)
    ("^\\*Backtrace" :vslot 99 :size 0.4 :quit nil)
    ("^\\*\\(?:Proced\\|timer-list\\|Process List\\|Abbrevs\\|Output\\|Occur\\|unsent mail\\)\\*" :ignore t)
    ("^\\*Flycheck errors\\*$"
     :vslot -2 :select t :quit t :ttl 0)))


;;; Shell integrations
(defun gagbo-find-file (file &optional line col)
  (find-file file)
  (when line
    ;; Following the manual which does like (goto-line) in non-interactive functions
    (goto-char (point-min))
    (forward-line (1- line)))
  (when col
    (forward-char col))
  (recenter))


;;; Bindings

(map!
 (:map evil-treemacs-state-map
       "M-n" #'multi-next-line
       "M-p" #'multi-previous-line)

;;;; :completion selectrum
 :n "M-c" #'embark-act
 (:map minibuffer-local-map
       "C-SPC" #'embark-act-noexit)

;;;; Avy
 :nvo "f" #'evilem-motion-find-char
 :nvo "F" #'evilem-motion-find-char-backward
 :nvo "t" #'evilem-motion-find-char-to
 :nvo "T" #'evilem-motion-find-char-to-backward
 (:leader
  :desc "Go to line above" "k" #'avy-goto-line-above
  :desc "Go to line below" "j" #'avy-goto-line-below
  :desc "Go to line" "l" #'avy-goto-line)
 "C-'" #'avy-goto-char-timer
 "C-:" #'avy-goto-word-or-subword-1

 (:leader
;;;; Quit
  (:prefix "q"
   :desc "Restart Emacs"                "r" #'doom/restart
   :desc "Restart & restore Emacs"      "R" #'doom/restart-and-restore
   :desc "Save buffers and kill server" "Q" #'save-buffers-kill-emacs)
;;;; Imenu
  (:prefix "o"
   :desc "Imenu list"                   "i" #'gagbo-imenu-toggle-maybe-lsp)
;;;; Toggles
  (:prefix "t"
   :desc "Read-only mode"               "r" #'read-only-mode
   :desc "Ghost (Transparency)" "G" #'gagbo/toggle-transparency)
;;;; Magit
  (:prefix "v"
   :desc "Magit file dispatch"          "d" #'magit-file-dispatch
   :desc "Create or checkout branch"    "b" #'magit-branch-or-checkout))

;;;; Hydra
 (:leader
  (:when (modulep! :ui hydra)
    (:prefix "w"
     :desc "Interactive menu"    "w" #'+hydra/window-nav/body)
    (:prefix ("z" . "zoom")
     :desc "Text"                "t" #'+hydra/text-zoom/body)))

;;;; Outshine
 (:after outshine
         (:map outshine-mode-map
               "TAB" #'outshine-cycle
               [tab] #'outshine-cycle
               [M-up] nil
               [M-left] nil
               [M-right] nil
               [M-down] nil))

;;;; Flycheck
 (:after flycheck
         (:map flycheck-mode-map
               "M-n" #'flycheck-next-error
               "M-p" #'flycheck-previous-error))

;;;; Languages
;;;;; Rust
 (:after rustic
         (:map rustic-mode-map
          :localleader
          (:prefix ("r" . "Rustic")
           :desc "Clippy pretty"     "C" #'rustic-cargo-clippy
           :desc "Popup"             "r" #'rustic-popup
           :desc "Format everything" "f" #'rustic-cargo-fmt
           :desc "Cargo-outdated"    "u" #'rustic-cargo-outdated)))

;;;;; Python
 (:after python
         (:map python-mode-map
          :localleader
          :desc "Blacken buffer" "cb" #'blacken-buffer)))


;;; Experimental zone

;;;; LSP Booster
(when (executable-find "emacs-lsp-booster") ; cargo install --git https://github.com/blahgeek/emacs-lsp-booster

  (defun lsp-booster--advice-json-parse (old-fn &rest args)
    "Try to parse bytecode instead of json."
    (or
     (when (equal (following-char) ?#)
       (let ((bytecode (read (current-buffer))))
         (when (byte-code-function-p bytecode)
           (funcall bytecode))))
     (apply old-fn args)))

  (advice-add (if (progn (require 'json)
                         (fboundp 'json-parse-buffer))
                  'json-parse-buffer
                'json-read)
              :around
              #'lsp-booster--advice-json-parse)

  (defun lsp-booster--advice-final-command (old-fn cmd &optional test?)
    "Prepend emacs-lsp-booster command to lsp CMD."
    (let ((orig-result (funcall old-fn cmd test?)))
      (if (and (not test?) ;; for check lsp-server-present?
               (not (file-remote-p default-directory)) ;; see lsp-resolve-final-command, it would add extra shell wrapper
               lsp-use-plists
               (not (functionp 'json-rpc-connection)) ;; native json-rpc
               (executable-find "emacs-lsp-booster"))
          (progn
            (message "Using emacs-lsp-booster for %s!" orig-result)
            (cons "emacs-lsp-booster" orig-result))
        orig-result)))

  (advice-add 'lsp-resolve-final-command :around #'lsp-booster--advice-final-command))

;;;; Multi vterm

(use-package! multi-vterm
  :custom
  (multi-vterm-buffer-name "Terminal")
  (multi-vterm-dedicated-window-side 'bottom)
  (multi-vterm-dedicated-buffer-name "Popup terminal")

  :config
  (map! :leader :desc "Dedicated terminal" "ot" #'multi-vterm-dedicated-toggle
        :leader :desc "Open terminal" "p!" #'multi-vterm-project)
  (map! (:map vterm-mode-map
         :localleader
         (:prefix ("m" . "Multi vterm")
          :desc "Create" "c" #'multi-vterm
          :desc "Previous" "p" #'multi-vterm-prev
          :desc "Next" "n" #'multi-vterm-next)))

  (set-popup-rules!
    '(("^\\*Terminal"
       :actions (display-buffer-in-side-window)
       :slot 2 :vslot -1 :side right :width 0.5 :quit nil))))

;; (evil-define-key 'normal vterm-mode-map (kbd "C-d")      #'vterm--self-insert)
;; (evil-define-key 'normal vterm-mode-map (kbd ",c")       #'multi-vterm)
;; (evil-define-key 'normal vterm-mode-map (kbd ",n")       #'multi-vterm-next)
;; (evil-define-key 'normal vterm-mode-map (kbd ",p")       #'multi-vterm-prev)
