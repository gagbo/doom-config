;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-
;; Place your private configuration here


;;; Early init
(let ((default-directory (expand-file-name "lisp" doom-private-dir)))
  (normal-top-level-add-subdirs-to-load-path))
(setq load-path (append
                 (mapcar (lambda (folder) (expand-file-name folder doom-private-dir))
                         '("lisp"))
                 load-path))
(load! "+local_config")



;;; Theme
;;;; Settings
(setq doom-theme 'doom-clapoto-dark
      doom-themes-treemacs-theme 'doom-colors
      doom-acario-dark-brighter-modeline t
      doom-acario-light-brighter-modeline t)

(setq current-theme doom-theme ; Use the dark theme first, better to flash dark->light than light->dark
      gagbo-light-theme 'doom-papercolor-light
      gagbo-dark-theme 'doom-clapoto-dark
      gagbo-light-theme-begin '(00 00 9)
      gagbo-light-theme-end '(00 00 20))

(run-with-timer 0 900 #'gagbo-circadian-theme)

;;;; HL-TODO faces
;; (setq hl-todo-keyword-faces
;;       `(("TODO"  . ,(face-foreground 'warning))
;;         ("FIXME" . ,(face-foreground 'error))
;;         ("NOTE"  . ,(face-foreground 'success))))

;;;; Rainbow mode with overlays
(use-package ov-rainbow-mode
  :commands ov-rainbow-mode)

;;;; Org and Treemacs configuration
(doom-themes-treemacs-config)
(doom-themes-org-config)

;;;; Modeline
;;;;; Light doom-modeline
(when (featurep! :ui modeline +light)
  (def-modeline! :gagbo
    '(""
      +modeline-matches
      " "
      +modeline-buffer-identification
      " "
      +modeline-position
      " "
      +modeline-modes
      (vc-mode ("  "
                ,(all-the-icons-octicon "git-branch" :v-adjust 0.0)
                vc-mode " "))
      (+modeline-checker ("" +modeline-checker "   "))
      )
    `(""
      mode-line-misc-info
      "  "
      +modeline-encoding
      "  "))
  (set-modeline! :gagbo 'default))



;;; Classic init
;;;; Window splitting settings
(setq evil-vsplit-window-right t
      evil-split-window-below t)

;;;; Global substitute
(setq evil-ex-substitute-global t)

;;;; Remove iedit message
(setq iedit-toggle-key-default nil)

;;;; Uniquify settings
(setq uniquify-buffer-name-style 'post-forward-angle-brackets
      uniquify-strip-common-suffix t)

;;;; Which key settings
(setq which-key-idle-delay 0.3)

;;;; Tabs and final EOL settings
(setq-default tab-width 8)
(setq require-final-newline t)

;;;; Ligatures settings
(setq +ligatures-in-modes (if IS-LINUX '(emacs-lisp-mode haskell-mode python-mode) nil))

;;;; Ophints settings
(setq evil-goggles-duration 0.2)

;;;; LSP settings
(set-eglot-client! 'python-mode `(,(concat doom-etc-dir "lsp/mspyls/Microsoft.Python.LanguageServer")))
(set-eglot-client! 'cc-mode '("clangd"))
(setq lua-lsp-dir "/home/gagbo/soft/lua-language-server/")
(setq lsp-headerline-breadcrumb-enable t)
(after! lsp
  (setq
   lsp-progress-via-spinner nil
   lsp-idle-delay 0.3
   lsp-headerline-breadcrumb-enable t
   lsp-print-performance nil
   lsp-enable-indentation t
   lsp-enable-on-type-formatting t
   lsp-enable-symbol-highlighting nil
   lsp-log-io nil))

(setq +lsp-company-backends '(company-capf :with company-yasnippet))

(after! lsp-ui
  (set-lookup-handlers! 'lsp-ui-mode nil)
  (setq lsp-ui-sideline-enable t
        lsp-ui-sideline-show-code-actions t
        lsp-ui-sideline-show-symbol nil
        lsp-ui-sideline-show-hover nil
        lsp-ui-sideline-show-diagnostics nil
        lsp-ui-doc-enable nil
        lsp-ui-doc-max-width 50
        lsp-ui-doc-max-height 15
        lsp-ui-doc-include-signature t
        lsp-ui-doc-header t
        lsp-document-highlight-delay 0.5)

  (add-hook! 'lsp-ui-mode-hook
    (run-hooks (intern (format "%s-lsp-ui-hook" major-mode)))))

;;;;; Flycheck settings
(after! flycheck
  (setq flycheck-display-errors-delay 0.1))



;;; Package configuration

;;;; Rainbow mode
;; Fontify html hex codes
(use-package rainbow-mode
  :diminish
  :commands rainbow-mode)

;;;; Key Quiz
(use-package! key-quiz
  :commands (key-quiz)
  :init
  (setq key-quiz-matching-regexp "^<?[MCSgz]"))

;;;; Aggressive Indent
(use-package! aggressive-indent
  :commands (aggressive-indent-mode))
;; Add hooks for messy code like (my) elisp and CSS
(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
(add-hook 'css-mode-hook #'aggressive-indent-mode)

;;;; Page Break Lines
(use-package! page-break-lines
  :init
  (setq page-break-lines-modes '(emacs-lisp-mode lisp-mode scheme-mode compilation-mode outline-mode help-mode))
  :commands (global-page-break-lines-mode page-break-lines-mode))

(global-page-break-lines-mode)

;;;; Flymake
(after! flymake
  (setq flymake-fringe-indicator-position 'left-fringe
        flymake-suppress-zero-counters t
        flymake-start-on-flymake-mode t
        flymake-no-changes-timeout nil
        flymake-start-on-save-buffer t
        flymake-proc-compilation-prevents-syntax-check t
        flymake-wrap-around nil))

;;;; Fish
(when (and (executable-find "fish")
           (require 'fish-completion nil t))
  (global-fish-completion-mode))

;;;; company
(after! company
  (setq company-global-modes '(not erc-mode message-mode help-mode gud-mode org-mode)
        company-idle-delay 0.0
        company-minimum-prefix-length 1))

(after! company-lsp
  (setq company-lsp-async t
        company-lsp-cache-candidates 'auto
        company-lsp-enable-snippet t
        company-lsp-enable-recompletion t))

;;;; ivy
(after! ivy-posframe (setq ivy-posframe-display-functions-alist
                           '((swiper          . nil)
                             (complete-symbol . ivy-posframe-display-at-point)
                             (counsel-M-x     . ivy-posframe-display-at-frame-top-center)
                             (counsel-rg      . ivy-posframe-display-at-frame-center)
                             (t               . ivy-posframe-display-at-frame-top-center))
                           ivy-posframe-border-width 1
                           ivy-posframe-parameters '((min-width . 90)
                                                     (min-height . 10)
                                                     (left-fringe . 8)
                                                     (right-fringe . 8))))

(after! swiper
  (setq swiper-goto-start-of-match t))

;;;; Helm
(after! helm
  (setq helm-display-header-line t
        helm-find-files-doc-header " (\\<helm-find-files-map>\\[helm-find-files-up-one-level]: Go up one level)"
        helm-mode-line-string "\
\\<helm-map>\
\\[helm-help]:Help \
\\[helm-select-action]:Act \
\\[helm-maybe-exit-minibuffer]/\
f1/f2/f-n:NthAct \
\\[helm-toggle-suspend-update]:Tog.suspend"
        helm-posframe-border-width 4
        helm-posframe-parameters '((min-width . 90)
                                   (min-height . 15)
                                   (border-width . 4)
                                   (left-fringe . 8)
                                   (right-fringe . 8))
        helm-posframe-poshandler #'posframe-poshandler-frame-top-center)
  ;; (helm-posframe-setup) ; For the posframe goodness
  )

;;;; Info colors
(use-package! info-colors
  :hook (Info-selection . info-colors-fontify-mode))

;;;; asciidoc
(use-package! adoc-mode
  :mode (("\\.asciidoc\\'" . adoc-mode)
         ("\\.asciidoc.fr\\'" . adoc-mode)
         ("\\.adoc.fr\\'" . adoc-mode)))

;;;; Jinja2
(use-package! jinja2-mode
  :mode (("\\.j2\\'" . jinja2-mode)
         ("\\.jinja\\'" . jinja2-mode)))

;;;; djinni
(use-package! djinni-mode
  :mode (("\\.djinni\\'" . djinni-mode)))

;;;; elisp
(use-package! flycheck-package
  :after flycheck
  :config
  (flycheck-package-setup))

;;;; go
(use-package! flycheck-golangci-lint
  :hook (go-mode . flycheck-golangci-lint-setup)
  :config (setenv "GO111MODULE" "on"))

(after! (go flycheck)
  (gagbo--go-flycheck-setup))

;;;; org
(load! "+org")

;;;; python
(after! lsp-python-ms
  (setq lsp-python-ms-nupkg-channel "beta"
        lsp-python-ms-executable
        (concat doom-etc-dir "lsp/mspyls/Microsoft.Python.LanguageServer")))

(after! lsp
  (setq lsp-pyls-plugins-pycodestyle-enabled nil ;; Disable to ensure sanity
        lsp-pyls-plugins-pylint-enabled nil ;; Disable to ensure performance
        lsp-pyls-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pyls-configuration-sources ["flake8"]))
(after! (python flycheck)
  (gagbo--python-flycheck-setup))

;;;; javascript (Flow)
(use-package company-flow
  :after company)
(with-eval-after-load 'company
  (add-to-list 'company-backends 'company-flow))

(use-package flycheck-flow
  :after flycheck)
(with-eval-after-load 'flycheck
  (gagbo--flow-flycheck-setup))

(use-package flow-minor-mode
  :hook (js2-mode . flow-minor-enable-automatically))

;;;; rust
(after! rustic
  (set-formatter! 'rustic-mode #'rustic-cargo-fmt))
(setq rustic-lsp-server 'rust-analyzer
      rustic-format-on-save t
      lsp-rust-server 'rust-analyzer)
(after! lsp
  (setq lsp-rust-analyzer-lru-capacity 10
        lsp-rust-analyzer-server-display-inlay-hints t
        lsp-rust-analyzer-display-chaining-hints t
        lsp-rust-analyzer-display-parameter-hints t
        lsp-rust-analyzer-cargo-watch-enable t
        lsp-rust-analyzer-cargo-watch-command "clippy"))

;;;; imenu-list
(use-package! imenu-list
  :commands imenu-list-smart-toggle)

;;;; Treemacs
(after! treemacs
  (set-popup-rule! "^ \\*Treemacs"
    :side 'left
    :size 0.30
    :quit nil
    :ttl 0))

;;;; Debugging
(use-package! realgud-lldb
  :defer t
  :init (add-to-list '+debugger--realgud-alist
                     '(realgud:lldb :modes (c-mode c++-mode rust-mode)
                                    :package realgud-lldb)))
;;;; Pkgbuild
(use-package! pkgbuild-mode
  :defer t
  :init
  (setq pkgbuild-update-sums-on-save nil)
  :config
  (add-hook! 'pkgbuild-mode-hook
    (setq mode-name "PKGBUILD"
          mode-line-process nil)))

;;; Additional Files
(load! "+faces")
(load! "+popups")
