;;; +init.el -*- lexical-binding: t; -*-

(setq custom-file (expand-file-name "custom.el" doom-private-dir))
(unless (file-exists-p custom-file)
  (write-region ";;; custom.el -*- lexical-binding: t; -*-" nil custom-file t))
(load custom-file)

;; Use Plists with LSP
(setenv "LSP_USE_PLISTS" "1")

(add-to-list 'default-frame-alist '(internal-border-width  . 20))
;; Rewriting localleader macro so that it adds stuff to XFK
;; (setq doom-localleader-key ".")
;; (setq doom-localleader-alt-key "M-SPC .")
;; (defmacro define-localleader-key! (&rest args)
;;   `(progn
;;      (general-define-key
;;       :keymaps 'xah-fly-command-map
;;       :major-modes t
;;       :prefix doom-localleader-key
;;       ,@args)
;;      (general-define-key
;;       :keymaps 'xah-fly-insert-map
;;       :major-modes t
;;       :prefix doom-localleader-alt-key
;;       ,@args)))
