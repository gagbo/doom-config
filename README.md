# Doom Emacs configuration

Private [Doom Emacs](https://github.com/hlissner/doom-emacs) configuration.

## Private modules

Adding private modules is as easy as adding them in the [modules](./modules)
folder, so this is my testing ground:
- before pushing PRs to Doom, or
- to easily toggle features
