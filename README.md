# Deprecation Warning
**This remote repository is archived**

**Thanks Framasoft for the hosting, I’m starting to move my projects to Sourcehut since Framagit will be over at some point and this repo is not essential at all.**

**New URL : https://git.sr.ht/~gagbo/doom-config**


----
----


# Doom Emacs configuration

Private [Doom Emacs](https://github.com/hlissner/doom-emacs) configuration.

## Private modules

Adding private modules is as easy as adding them in the [modules](./modules)
folder, so this is my testing ground :
- before pushing PRs to Doom, or
- to easily toggle features

### Bindings

I add my own bindings in a module. I used to freeze the bindings from
~modules/config/default/+evil-bindings.el~, in order to have fixed bindings
that I update at my own rhythm, but it makes little sense to keep all default
there. It now only contains my overrides and might soon move back in config.el
and out of modules

### Completion

#### Selectrum
Trying out selectrum from time to time to see if I prefer it to Ivy or Helm

### Config

#### Smartparens

This is the ~modules/config/default +smartparens~ part I copied in order to
control the changes in smartparens behaviour. I will probably deprecate it
soon.

### Lang

#### Beancount

Support for Beancount files. Basically just including the beacount major mode
from beancount source code (I changed the base mode be cause I target newer
emacs).

I am also trying to add `bean-format` as a formatter. This is a WIP.

#### Powershell (basic module example)

Support for powershell. Most basic module ever, if anyone wants to see how to
"package" a plugin and a simple config in a module.

#### Fennel (basic module example)

Support for [Fennel](https://fennel-lang.org/). It is also a pretty basic
module, and if it ever gets upstreamed it should be a flag of the current Lua
module like Moonscript

### Misc.

#### Elcord (basic module example)

Rich presence in Discord (get the /playing Doom Emacs/ status)

#### C/C++ module

Inspired by maskRay, I keep my cc configuration apart. Mostly just a few
overrides of settings (for GDB / Projectile), and if `lsp-mode` is used (not
`eglot`), then the `ccls` configuration kicks in too, adding a few functions
wrote by maskRay for code navigation in the ccls-tree mode.

Those changes are currently being added in Doom where relevant

#### Transient (Spacemacs-like)

Use [Hercules.el](https://gitlab.com/jjzmajic/hercules.el) to get "transient
states" like Spacemacs advertises. Currently this is just a test module to see
how it could be integrated in Doom. I only use it for buffers and for windows
(and not even that often)

### Tools

#### Direnv

A copy of the WIP direnv module rewrite

#### FZF

A try to integrate [fzf.el](https://github.com/bling/fzf.el) directly in Doom,
its architecture (i.e. maintain the fzf process with a subjob in emacs) is
better for the filtering of large lists of candidates

#### Tree Sitter

A draft to include [Tree Sitter](https://tree-sitter.github.io/tree-sitter/)
into Doom. This currently works "well enough" for my setup, but this is the
kind of module which is hard to PR because of the width of the scope and
support issue it might generate

### UI

#### Telephone line

I use telephone line as my modeline. I mostly copied some modeline formats from
the default :ui modeline module, and made them work nicely with telephone-line.

The arbritrary choice of separators conviced me to go for it.

