;;; themes/doom-rose-pine-dawn-theme.el -*- lexical-binding: t;no-byte-compile: t -*-

;;; Commentary:
(require 'doom-themes)
;;; Code:
;;
(defgroup doom-rose-pine-dawn-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-rose-pine-dawn-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-rose-pine-dawn-theme
  :type 'boolean)

(defcustom doom-rose-pine-dawn-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-rose-pine-dawn-theme
  :type 'boolean)

(defcustom doom-rose-pine-dawn-comment-bg doom-rose-pine-dawn-brighter-comments
  "If non-nil, comments will have a subtle, darker background.
Enhancing their legibility."
  :group 'doom-rose-pine-dawn-theme
  :type 'boolean)

(defcustom doom-rose-pine-dawn-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'doom-rose-pine-dawn-theme
  :type '(choice integer boolean))

(def-doom-theme doom-rose-pine-dawn
  "A sepia/green light theme"

  ((bg         '("#faf4ed" "color-231")) ; Base
   (bg-alt     '("#f4ede8" "color-255")) ; Highlight Low
   (base0      '("#fffaf3" "color-231")) ; Surface
   (base1      '("#fffaf3" "color-231")) ; Surface
   (base2      '("#f2e9e1" "color-255")) ; Overlay
   (base3      '("#dfdad9" "color-253")) ; Highlight Med
   (base4      '("#cecacd" "color-252")) ; Highlight High
   (base5      '("#9893a5" "color-103")) ; Muted
   (base6      '("#9893a5" "color-103")) ; Muted
   (base7      '("#797593" "color-102")) ; Subtle
   (base8      '("#575279" "color-60"))  ; Text
   (fg         '("#575279" "color-60"))  ; Text
   (fg-alt     '("#797593" "color-102")) ; Subtle

   (grey        base6)
   (red         '("#b4637a" "color-132")) ; Love
   (orange      '("#ab704f" "color-137")) ; Non-standard
   (green       '("#286983" "color-24"))  ; Pine
   (green-br    '("#b6d1de" "color-152")) ; Non-standard
   (teal        '("#d7827e" "color-174")) ; Rose
   (yellow      '("#ea9d34" "color-215")) ; Gold
   (dark-yellow '("#dcc9b3" "color-187")) ; Non-standard
   (blue        '("#56949f" "color-73"))  ; Foam
   (dark-blue   '("#b2d3d9" "color-152")) ; Non-standard
   (magenta     '("#907aa9" "color-103")) ; Iris
   (violet      '("#907aa9" "color-103")) ; Iris
   (cyan        '("#d7827e" "color-174")) ; Rose
   (dark-cyan   '("#e1c5c3" "color-254")) ; Non-standard
   ;; face categories
   (highlight      orange)
   (vertical-bar   bg-alt)
   (selection      base3)
   (builtin        red)
   (comments       (if doom-rose-pine-dawn-brighter-comments green grey))
   (doc-comments   (if doom-rose-pine-dawn-brighter-comments blue (doom-darken blue 0.1)))
   (constants      cyan)
   (functions      green)
   (keywords       blue)
   (methods        green)
   (operators      fg)
   (type           green)
   (strings        yellow)
   (variables      fg)
   (numbers        cyan)
   (region         base2)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    cyan)
   (vc-added       blue)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (hidden-alt `(,(car bg-alt) "black" "black"))
   (-modeline-pad
    (when doom-rose-pine-dawn-padded-modeline
      (if (integerp doom-rose-pine-dawn-padded-modeline) doom-sourcerer-padded-modeline 4)))

   (modeline-fg base8)
   (modeline-fg-alt (doom-blend yellow grey (if doom-rose-pine-dawn-brighter-modeline 0.4 0.08)))

   (modeline-bg
    (if doom-rose-pine-dawn-brighter-modeline
        base1
      base2))
   (modeline-bg-l
    (if doom-rose-pine-dawn-brighter-modeline
        modeline-bg
      `(,(doom-darken (car bg) 0.15) ,@(cdr base1))))
   (modeline-bg-inactive   (doom-darken bg 0.20))
   (modeline-bg-inactive-l `(,(doom-darken (car bg-alt) 0.2) ,@(cdr base0))))

  ;; --- extra faces ------------------------
  ((elscreen-tab-other-screen-face :background base8 :foreground base4)
   (cursor :background base4)
   (font-lock-comment-face
    :foreground comments
    :background (if doom-rose-pine-dawn-comment-bg base1))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)
   (mode-line-buffer-id :foreground blue :bold bold)
   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground teal :bold bold)

   (doom-modeline-bar :background (if doom-rose-pine-dawn-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-path :foreground (if doom-rose-pine-dawn-brighter-modeline base8 blue) :bold bold)
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-project-root :foreground teal :weight 'bold)

   (tab-line :background base3)
   (tab-line-tab :background base3 :foreground fg)
   (tab-line-tab-inactive :background base3 :foreground fg-alt :box `(:line-width 1 :color ,base1 :style released-button))
   (tab-line-tab-current :background base4 :foreground fg :box `(:line-width 1 :color ,base1 :style pressed-button))
   (tab-bar :background base2)
   (tab-bar-tab :background base4 :foreground highlight)
   (tab-bar-tab-inactive :background bg-alt :foreground fg-alt)

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,base3)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if doom-rose-pine-dawn-brighter-modeline base8 highlight))
   (fringe :background bg)
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   (lazy-highlight :foreground fg :background base4)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground yellow)

;;;;; ivy-mode
   (ivy-current-match :background dark-yellow :distant-foreground base7 :weight 'normal)
   (ivy-posframe :background base0 :foreground fg)
   (internal-border :background bg)

;;;;; lsp-mode and lsp-ui-mode
   (lsp-face-highlight-textual :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-face-highlight-read    :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-face-highlight-write   :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-ui-peek-header :foreground fg :background (doom-darken bg 0.1) :bold bold)
   (lsp-ui-peek-list :background (doom-lighten bg 0.1))
   (lsp-ui-peek-peek :background (doom-lighten bg 0.1))

   ;; tooltip and company
   (tooltip              :background bg-alt :foreground fg)
   (company-tooltip-selection     :background base3)

   ;; markdown-mode
   (markdown-header-face :inherit 'bold :foreground red)
   ;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face :foreground dark-yellow)
   (rainbow-delimiters-depth-2-face :foreground green)
   (rainbow-delimiters-depth-3-face :foreground orange)
   (rainbow-delimiters-depth-4-face :foreground red)
   (rainbow-delimiters-depth-5-face :foreground yellow)
   (rainbow-delimiters-depth-6-face :foreground orange)
   (rainbow-delimiters-depth-7-face :foreground teal)

;;;;; org-mode
   ((org-block &override) :background bg-alt)
   ((org-block-begin-line &override) :background bg-alt :slant 'italic)
   ((org-block-end-line &override) :background bg-alt :slant 'italic)
   ((org-document-title &override) :foreground yellow :height 1.953125)
   ((org-level-1 &override) :foreground blue :height 1.75)
   ((org-level-2 &override) :foreground green :height 1.5625)
   ((org-level-3 &override) :foreground orange :height 1.25)
   ((org-level-4 &override) :foreground red)
   ((org-level-5 &override) :foreground magenta)
   ((org-level-6 &override) :foreground yellow)
   ((org-level-7 &override) :foreground violet)
   ((org-level-8 &override) :foreground green)
   (org-hide :foreground hidden)
   ((org-quote &override) :background base1)
   (solaire-org-hide-face :foreground hidden-alt)

;;;;; rjsx-mode
   (rjsx-tag :foreground yellow)
   (rjsx-tag-bracket-face :foreground base8)
   (rjsx-attr :foreground magenta :slant 'italic :weight 'medium)

;;;;; selectrum
   (selectrum-current-candidate :background base3 :distant-foreground base7 :weight 'normal)

;;;;; treemacs
   (treemacs-root-face :foreground strings :weight 'bold :height 1.2)
   (doom-themes-treemacs-file-face :foreground comments)
   )


  ;; --- extra variables --------------------
  ;; ()

  )

;;; doom-rose-pine-dawn-theme.el ends here
