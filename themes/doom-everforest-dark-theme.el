;;; doom-everforest-dark-theme.el -*- lexical-binding: t;no-byte-compile: t -*-

;;; Commentary:
(require 'doom-themes)
;;; Code:
;;
(defgroup doom-everforest-dark-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-everforest-dark-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-everforest-dark-theme
  :type 'boolean)

(defcustom doom-everforest-dark-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-everforest-dark-theme
  :type 'boolean)

(defcustom doom-everforest-dark-comment-bg doom-everforest-dark-brighter-comments
  "If non-nil, comments will have a subtle, darker background.
Enhancing their legibility."
  :group 'doom-everforest-dark-theme
  :type 'boolean)

(defcustom doom-everforest-dark-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'doom-everforest-dark-theme
  :type '(choice integer boolean))

;; Source https://github.com/sainnhe/everforest/blob/64f486a4856bfbfd6604c700c938b34cf551561b/autoload/everforest.vim
;; The background/foreground values have been tweaked to give harder contrast
(def-doom-theme doom-everforest-dark
  "A port of sainnhe/everforest (dark hard) theme."

  ((bg         '("#111416" "color-16"))
   (bg-alt     '("#202529" "color-235"))
   (base0      '("#323C41" "color-236"))
   (base1      '("#3A454A" "color-237"))
   (base2      '("#445055" "color-238"))
   (base3      '("#45555B" "color-239"))
   (base4      '("#53605C" "color-240"))
   (base5      '("#7A8478" "color-243"))
   (base6      '("#859289" "color-245"))
   (base7      '("#9DA9A0" "color-247"))
   (base8      '("#E8D9BB" "color-230"))
   (fg         '("#D3C6AA" "color-230"))
   (fg-alt     '("#FBEBCA" "color-230"))

   (visual         '("#503946" "color-52"))
   (red-bg         '("#4E3E43" "color-52"))
   (green-bg       '("#404D44" "color-22"))
   (blue-bg        '("#394F5A" "color-17"))
   (yellow-bg      '("#4A4940" "color-136"))

   (grey        '("#ADA28B" "color-243"))
   (red         '("#E67E80" "color-204"))
   (orange      '("#E69875" "color-172"))
   (green       '("#A7C080" "color-191"))
   (green-br    '("#92b34e" "color-107"))
   (teal        '("#83C092" "color-66"))
   (yellow      '("#DBBC7F" "color-185"))
   (dark-yellow yellow-bg)
   (blue        '("#7FBBB3" "color-110"))
   (dark-blue   '("#4e608e" "color-60"))
   (magenta     '("#D699B6" "color-218"))
   (violet      magenta)
   (cyan        '("#83C0B9" "color-159"))
   (dark-cyan   blue-bg)
   ;; face categories
   (highlight      magenta)
   (vertical-bar   base0)
   (selection      visual)
   (builtin        orange)
   (comments       (if doom-everforest-dark-brighter-comments green-br grey))
   (doc-comments   fg)
   (constants      fg)
   (functions      fg)
   (keywords       red)
   (methods        orange)
   (operators      cyan)
   (type           cyan)
   (strings        green)
   (variables      fg)
   (numbers        fg)
   (region         base2)
   (error          red)
   (warning        orange)
   (success        green)
   (vc-modified    teal)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (hidden-alt `(,(car bg-alt) "black" "black"))
   (-modeline-pad
    (when doom-everforest-dark-padded-modeline
      (if (integerp doom-everforest-dark-padded-modeline) doom-everforest-dark-padded-modeline 4)))

   (modeline-fg base7)
   (modeline-fg-alt (doom-blend yellow grey (if doom-everforest-dark-brighter-modeline 0.4 0.08)))

   (modeline-bg
    (if doom-everforest-dark-brighter-modeline
        base2
      base1))
   (modeline-bg-l
    (if doom-everforest-dark-brighter-modeline
        modeline-bg
      `(,(doom-darken (car bg) 0.15) ,@(cdr base1))))
   (modeline-bg-inactive  (doom-darken bg 0.20))
   (modeline-bg-inactive-l `(,(doom-darken (car bg-alt) 0.2) ,@(cdr base0))))

  ;; --- extra faces ------------------------
  ((elscreen-tab-other-screen-face :background base4 :foreground base1)
   (cursor :background base4)
   (font-lock-comment-face
    :foreground comments)
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments
    :background green-bg)
   (mode-line-buffer-id :foreground green-br :bold bold)
   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground teal :bold bold)

   (doom-modeline-bar :background (if doom-everforest-dark-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-path :foreground (if doom-everforest-dark-brighter-modeline base8 blue) :bold bold)
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-project-root :foreground teal :weight 'bold)

   (tab-line :background base3)
   (tab-line-tab :background base3 :foreground fg)
   (tab-line-tab-inactive :background base3 :foreground fg-alt :box `(:line-width 1 :color ,base1 :style released-button))
   (tab-line-tab-current :background base4 :foreground fg :box `(:line-width 1 :color ,base1 :style pressed-button))
   (tab-bar :background base2)
   (tab-bar-tab :background base4 :foreground highlight)
   (tab-bar-tab-inactive :background bg-alt :foreground fg-alt)

   (mode-line
    :background base3 :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,base3)))
   (mode-line-inactive
    :background bg-alt :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if doom-everforest-dark-brighter-modeline base8 highlight))
   (fringe :background bg)
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   (lazy-highlight :foreground fg :background blue-bg)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground yellow)

;;;;; ivy-mode
   (ivy-current-match :background base3 :distant-foreground base7 :weight 'normal)
   (ivy-posframe :background base0 :foreground fg)
   (internal-border :background bg)

;;;;; lsp-mode and lsp-ui-mode
   (lsp-face-highlight-textual :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-face-highlight-read    :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-face-highlight-write   :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-ui-peek-header :foreground fg :background (doom-lighten bg 0.1) :bold bold)
   (lsp-ui-peek-list :background (doom-darken bg 0.1))
   (lsp-ui-peek-peek :background (doom-darken bg 0.1))

   ;; tooltip and company
   (tooltip              :background bg-alt :foreground fg)
   (company-tooltip-selection     :background base3)

   ;; markdown-mode
   (markdown-header-face :inherit 'bold :foreground red)

   ;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face :foreground yellow)
   (rainbow-delimiters-depth-2-face :foreground green-br)
   (rainbow-delimiters-depth-3-face :foreground teal)
   (rainbow-delimiters-depth-4-face :foreground orange)
   (rainbow-delimiters-depth-5-face :foreground magenta)
   (rainbow-delimiters-depth-6-face :foreground yellow)
   (rainbow-delimiters-depth-7-face :foreground orange)
;;;;; org-mode
   ((org-block &override) :background bg-alt)
   ((org-block-begin-line &override) :background bg-alt :slant 'italic)
   ((org-block-end-line &override) :background bg-alt :slant 'italic)
   ((org-document-title &override) :foreground yellow :height 1.953125)
   ((org-level-1 &override) :foreground blue :height 1.75)
   ((org-level-2 &override) :foreground green-br :height 1.5625)
   ((org-level-3 &override) :foreground yellow :height 1.25)
   ((org-level-4 &override) :foreground red)
   ((org-level-5 &override) :foreground magenta)
   ((org-level-6 &override) :foreground violet)
   ((org-level-7 &override) :foreground cyan)
   ((org-level-8 &override) :foreground magenta)
   (org-hide :foreground hidden)
   ((org-quote &override) :background base1)
   (solaire-org-hide-face :foreground hidden-alt)

;;;;; rjsx-mode
   (rjsx-tag :foreground yellow)
   (rjsx-tag-bracket-face :foreground base8)
   (rjsx-attr :foreground magenta :slant 'italic :weight 'medium)

;;;;; selectrum
   (selectrum-current-candidate :background base3 :distant-foreground base7 :weight 'normal)

;;;;; treemacs
   (treemacs-root-face :foreground strings :weight 'bold :height 1.2)
   (doom-themes-treemacs-file-face :foreground comments)
   )


  ;; --- extra variables --------------------
  ;; ()

  )

;;; doom-everforest-dark-theme.el ends here
