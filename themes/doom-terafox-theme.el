;;; doom-terafox-theme.el --- A port of Nightfox.nvim Terafox variant -*- lexical-binding: t; -*-

;; Added:
;; Author: gagbo <https://github.com/gagbo>
;; Maintainer:
;; Source: https://github.com/EdenEast/nightfox.nvim/tree/main
;; Source: https://github.com/EdenEast/nightfox.nvim/blob/main/lua/nightfox/palette/terafox.lua
;;
;;; Commentary:
;;; Code:

(require 'doom-themes)

;;
;;; Variables

(defgroup doom-terafox-theme nil
  "Options for the `doom-terafox' theme."
  :group 'doom-themes)

(defcustom doom-terafox-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-terafox-theme
  :type 'boolean)

(defcustom doom-terafox-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'doom-terafox-theme
  :type '(choice integer boolean))


;;
;;; Theme definition

(def-doom-theme doom-terafox
    "A port of the Terafox variant of Nightfox."

  ;; name        default   256       16
  (
   ;; Terafox palette
   (tf-black-base     '("#2F3239"))
   (tf-black-bright   '("#4E5157"))
   (tf-black-dim      '("#282A30"))
   (tf-red-base       '("#E85C51"))
   (tf-red-bright     '("#EB746B"))
   (tf-red-dim        '("#C54E45"))
   (tf-green-base     '("#7AA4A1"))
   (tf-green-bright   '("#8EB2AF"))
   (tf-green-dim      '("#688B89"))
   (tf-yellow-base    '("#FDA47F"))
   (tf-yellow-bright  '("#FDB292"))
   (tf-yellow-dim     '("#D78B6C"))
   (tf-blue-base      '("#5A93AA"))
   (tf-blue-bright    '("#73A3B7"))
   (tf-blue-dim       '("#4D7D90"))
   (tf-magenta-base   '("#AD5C7C"))
   (tf-magenta-bright '("#B97490"))
   (tf-magenta-dim    '("#934E69"))
   (tf-cyan-base      '("#A1CDD8"))
   (tf-cyan-bright    '("#AFD4DE"))
   (tf-cyan-dim       '("#89AEB8"))
   (tf-white-base     '("#EBEBEB"))
   (tf-white-bright   '("#EEEEEE"))
   (tf-white-dim      '("#C8C8C8"))
   (tf-orange-base    '("#FF8349"))
   (tf-orange-bright  '("#FF9664"))
   (tf-orange-dim     '("#D96F3E"))
   (tf-pink-base      '("#CB7985"))
   (tf-pink-bright    '("#D38D97"))
   (tf-pink-dim       '("#AD6771"))

   (tf-comment        '("#6D7F8B"))

   (tf-bg0            '("#0F1C1E"))     ; Dark BG (status line, floats)
   (tf-bg1            '("#152528"))     ; Default BG
   (tf-bg2            '("#1D3337"))     ; Lighter BG (colorcolm, folds)
   (tf-bg3            '("#254147"))     ; Lighter BG (cursorline)
   (tf-bg4            '("#2D4F56"))     ; Conceal, border FG

   (tf-fg0            '("#EAEEEE"))     ; Lighter FG
   (tf-fg1            '("#E6EAEA"))     ; Default FG
   (tf-fg2            '("#CBD9D8"))     ; Darker FG (status line)
   (tf-fg3            '("#587B7B"))     ; Darker BG (line numbers, fold columns)

   (tf-sel0           '("#293E40"))     ; Popup BG, visual selection BG
   (tf-sel1           '("#425E5E"))     ; Popup Sel BG, Search BG

   ;; Doom palette

   (bg tf-bg1)
   (fg tf-fg1)

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt tf-bg3)
   (fg-alt tf-fg3)

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0 tf-bg0)
   (base1 tf-bg0)
   (base2 tf-bg1)
   (base3 tf-bg1)
   (base4 tf-bg2)
   (base5 tf-fg3)
   (base6 tf-fg2)
   (base7 tf-fg1)
   (base8 tf-fg0)

   (grey       tf-black-bright)
   (red tf-red-base)
   (orange tf-orange-base)
   (green tf-green-base)
   (teal tf-cyan-base)
   (yellow tf-yellow-base)
   (blue tf-blue-base)
   (dark-blue tf-blue-dim)
   (magenta tf-magenta-base)
   (violet tf-pink-base)
   (cyan tf-cyan-base)
   (dark-cyan tf-cyan-dim)

   ;; face categories -- required for all themes
   (highlight      tf-blue-bright)
   (vertical-bar tf-bg4)
   (selection      tf-sel0)
   (builtin        tf-cyan-dim)
   (comments       (if doom-terafox-brighter-comments tf-cyan-dim tf-comment))
   (doc-comments   tf-cyan-bright)
   (constants      tf-orange-bright)
   (functions      tf-blue-bright)
   (keywords       tf-magenta-base)
   (methods        tf-blue-base)
   (operators      tf-fg2)
   (type           tf-yellow-base)
   (strings        tf-green-base)
   (variables      tf-white-base)
   (numbers        tf-orange-base)
   (region tf-sel1)
   (error          tf-red-base)
   (warning        tf-yellow-base)
   (info           tf-blue-base)
   (hint           tf-green-base)
   (success        tf-green-base)
   (vc-modified    tf-yellow-dim)
   (vc-added       tf-green-dim)
   (vc-deleted     tf-red-dim)

   ;; Backgrounds
   ;; Diagnostics bg
   ;; (tf-diag-bg-error (doom-blend tf-bg1 error 0.15))
   ;; (tf-diag-bg-warning (doom-blend tf-bg1 warning 0.15))
   ;; (tf-diag-bg-info (doom-blend tf-bg1 info 0.15))
   ;; (tf-diag-bg-hint (doom-blend tf-bg1 hint 0.15))
   ;; (tf-diag-bg-success (doom-blend tf-bg1 success 0.15))
   ;;
   ;; Diff bg
   ;; (tf-diff-add (doom-blend tf-bg1 tf-green-dim 0.2))
   ;; (tf-diff-delete (doom-blend tf-bg1 tf-red-dim 0.2))
   ;; (tf-diff-change (doom-blend tf-bg1 tf-cyan-dim 0.2))
   ;; (tf-diff-text (doom-blend tf-bg1 tf-cyan-base 0.35))

   ;; custom categories
   (-modeline-pad
    (when doom-terafox-padded-modeline
      (if (integerp doom-terafox-padded-modeline) doom-terafox-padded-modeline 4)))

   (modeline-fg     fg)
   (modeline-fg-alt base5)

   (modeline-bg base3)
   (modeline-bg-alt base3)
   (modeline-bg-inactive bg-alt)
   (modeline-bg-inactive-alt (doom-darken bg 0.1)))


  ;;;; Base theme face overrides
  (((font-lock-comment-face &override)
    :background (if doom-terafox-brighter-comments (doom-lighten bg 0.05) 'unspecified))
   ((line-number &override) :foreground tf-fg3)
   ((line-number-current-line &override) :foreground fg)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground highlight)

   ;;;; centaur-tabs
   (centaur-tabs-active-bar-face :background blue)
   (centaur-tabs-modified-marker-selected
    :inherit 'centaur-tabs-selected :foreground blue)
   (centaur-tabs-modified-marker-unselected
    :inherit 'centaur-tabs-unselected :foreground blue)
   ;;;; company
   (company-tooltip-selection     :background dark-cyan)
   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)
   ;;;; doom-modeline
   (doom-modeline-bar :background blue)
   (doom-modeline-evil-emacs-state  :foreground magenta)
   (doom-modeline-evil-insert-state :foreground blue)
   ;;;; helm
   (helm-selection :inherit 'bold
                   :background selection
                   :distant-foreground bg
                   :extend t)
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   (markdown-url-face    :foreground teal :weight 'normal)
   (markdown-reference-face :foreground base6)
   ((markdown-bold-face &override)   :foreground fg)
   ((markdown-italic-face &override) :foreground fg-alt)
   ((markdown-code-face &override)   :background bg-alt)
   ;;;; outline <built-in>
   ((outline-1 &override) :foreground blue)
   ((outline-2 &override) :foreground green)
   ((outline-3 &override) :foreground teal)
   ((outline-4 &override) :foreground (doom-darken blue 0.2))
   ((outline-5 &override) :foreground (doom-darken green 0.2))
   ((outline-6 &override) :foreground (doom-darken teal 0.2))
   ((outline-7 &override) :foreground (doom-darken blue 0.4))
   ((outline-8 &override) :foreground (doom-darken green 0.4))
   ;;;; org <built-in>
   ((org-block &override) :background base0)
   ((org-block-begin-line &override) :foreground comments :background base0)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-alt)))))

  ;;;; Base theme variable overrides-
;; ()


;;; doom-terafox-theme.el ends here
