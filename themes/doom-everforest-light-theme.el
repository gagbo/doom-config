;;; themes/doom-everforest-light-theme.el -*- lexical-binding: t;no-byte-compile: t -*-

;;; Commentary:
(require 'doom-themes)
;;; Code:
;;
(defgroup doom-everforest-light-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-everforest-light-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-everforest-light-theme
  :type 'boolean)

(defcustom doom-everforest-light-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-everforest-light-theme
  :type 'boolean)

(defcustom doom-everforest-light-comment-bg doom-everforest-light-brighter-comments
  "If non-nil, comments will have a subtle, darker background.
Enhancing their legibility."
  :group 'doom-everforest-light-theme
  :type 'boolean)

(defcustom doom-everforest-light-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'doom-everforest-light-theme
  :type '(choice integer boolean))

;; Source https://github.com/sainnhe/everforest/blob/64f486a4856bfbfd6604c700c938b34cf551561b/autoload/everforest.vim
;; The background/foreground values have been tweaked to give harder contrast
(def-doom-theme doom-everforest-light
  "A port of sainnhe/everforest theme (light hard)"

  ((bg         '("#FFF9E8" "color-231"))
   (bg-alt     '("#F7F4E0" "color-194"))
   (base0      '("#F0EED9" "color-194"))
   (base1      '("#E9E8D2" "color-188"))
   (base2      '("#E1DDCB" "color-151"))
   (base3      '("#BEC5B2" "color-145"))
   (base4      '("#798C96" "color-151"))
   (base5      '("#5C6A72" "color-108"))
   (base6      '("#4E5A60" "color-240"))
   (base7      '("#3B4449" "color-65"))
   (base8      '("#1c2123" "color-235"))
   (fg         '("#2F363A" "color-16"))
   (fg-alt     '("#406072" "color-22"))

   (visual      '("#EDF0CD" "color-194"))
   (red-bg      '("#FCE5DC" "color-217"))
   (green-bg    '("#F1F3D4" "color-194"))
   (blue-bg     '("#EAF2EB" "color-117"))
   (yellow-bg   '("#FBEFD0" "color-226"))

   (grey        '("#5C6A72" "color-240"))
   (red         '("#F85552" "color-124"))
   (orange      '("#F57D26" "color-94"))
   (green       '("#8DA101" "color-28"))
   (green-br    '("#50a05b" "color-71"))
   (teal        '("#35A77C" "color-68"))
   (yellow      '("#DFA000" "color-100"))
   (dark-yellow yellow-bg)
   (blue        '("#3A94C5" "color-18"))
   (dark-blue   blue-bg)
   (magenta     '("#DF69BA" "color-89"))
   (violet      magenta)
   (cyan        '("#359EA7" "color-29"))
   (dark-cyan   blue-bg)
   ;; face categories
   (highlight      magenta)
   (vertical-bar   base0)
   (selection      visual)
   (builtin        orange)
   (comments       (if doom-everforest-light-brighter-comments green-br grey))
   (doc-comments   fg)
   (constants      fg)
   (functions      fg)
   (keywords       red)
   (methods        orange)
   (operators      cyan)
   (type           cyan)
   (strings        green)
   (variables      fg)
   (numbers        fg)
   (region         base2)
   (error          red)
   (warning        orange)
   (success        green)
   (vc-modified    teal)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (hidden-alt `(,(car bg-alt) "black" "black"))
   (-modeline-pad
    (when doom-everforest-light-padded-modeline
      (if (integerp doom-everforest-light-padded-modeline) doom-everforest-light-padded-modeline 4)))

   (modeline-fg base8)
   (modeline-fg-alt (doom-blend yellow grey (if doom-everforest-light-brighter-modeline 0.4 0.08)))

   (modeline-bg
    (if doom-everforest-light-brighter-modeline
        base2
      base1))
   (modeline-bg-l
    (if doom-everforest-light-brighter-modeline
        modeline-bg
      `(,(doom-darken (car bg) 0.15) ,@(cdr base1))))
   (modeline-bg-inactive  (doom-darken bg 0.20))
   (modeline-bg-inactive-l `(,(doom-darken (car bg-alt) 0.2) ,@(cdr base0))))

  ;; --- extra faces ------------------------
  ((elscreen-tab-other-screen-face :background base8 :foreground base4)
   (cursor :background base4)
   (font-lock-comment-face
    :foreground comments)
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments
    :background green-bg)
   (mode-line-buffer-id :foreground blue :bold bold)
   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground teal :bold bold)

   (doom-modeline-bar :background (if doom-everforest-light-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-path :foreground (if doom-everforest-light-brighter-modeline base8 blue) :bold bold)
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-project-root :foreground teal :weight 'bold)

   (tab-line :background base3)
   (tab-line-tab :background base3 :foreground fg)
   (tab-line-tab-inactive :background base3 :foreground fg-alt :box `(:line-width 1 :color ,base1 :style released-button))
   (tab-line-tab-current :background base4 :foreground fg :box `(:line-width 1 :color ,base1 :style pressed-button))
   (tab-bar :background base2)
   (tab-bar-tab :background base4 :foreground highlight)
   (tab-bar-tab-inactive :background bg-alt :foreground fg-alt)

   (mode-line
    :background base3 :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,base3)))
   (mode-line-inactive
    :background bg-alt :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if doom-everforest-light-brighter-modeline base8 highlight))
   (fringe :background bg)
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   (lazy-highlight :foreground fg :background blue-bg)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground yellow)

;;;;; ivy-mode
   (ivy-current-match :background dark-yellow :distant-foreground base7 :weight 'normal)
   (ivy-posframe :background base0 :foreground fg)
   (internal-border :background bg)

;;;;; lsp-mode and lsp-ui-mode
   (lsp-face-highlight-textual :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-face-highlight-read    :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-face-highlight-write   :background dark-cyan :foreground base8 :distant-foreground base7 :weight 'bold)
   (lsp-ui-peek-header :foreground fg :background (doom-darken bg 0.1) :bold bold)
   (lsp-ui-peek-list :background (doom-lighten bg 0.1))
   (lsp-ui-peek-peek :background (doom-lighten bg 0.1))

   ;; tooltip and company
   (tooltip              :background bg-alt :foreground fg)
   (company-tooltip-selection     :background base3)

   ;; markdown-mode
   (markdown-header-face :inherit 'bold :foreground red)
   ;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face :foreground yellow)
   (rainbow-delimiters-depth-2-face :foreground green)
   (rainbow-delimiters-depth-3-face :foreground orange)
   (rainbow-delimiters-depth-4-face :foreground magenta)
   (rainbow-delimiters-depth-5-face :foreground yellow)
   (rainbow-delimiters-depth-6-face :foreground orange)
   (rainbow-delimiters-depth-7-face :foreground teal)

;;;;; org-mode
   ((org-block &override) :background bg-alt)
   ((org-block-begin-line &override) :background bg-alt :slant 'italic)
   ((org-block-end-line &override) :background bg-alt :slant 'italic)
   ((org-document-title &override) :foreground yellow :height 1.953125)
   ((org-level-1 &override) :foreground blue :height 1.75)
   ((org-level-2 &override) :foreground green :height 1.5625)
   ((org-level-3 &override) :foreground orange :height 1.25)
   ((org-level-4 &override) :foreground red)
   ((org-level-5 &override) :foreground magenta)
   ((org-level-6 &override) :foreground yellow)
   ((org-level-7 &override) :foreground violet)
   ((org-level-8 &override) :foreground green)
   (org-hide :foreground hidden)
   ((org-quote &override) :background base1)
   (solaire-org-hide-face :foreground hidden-alt)

;;;;; rjsx-mode
   (rjsx-tag :foreground yellow)
   (rjsx-tag-bracket-face :foreground base8)
   (rjsx-attr :foreground magenta :slant 'italic :weight 'medium)

;;;;; selectrum
   (selectrum-current-candidate :background base3 :distant-foreground base7 :weight 'normal)

;;;;; treemacs
   (treemacs-root-face :foreground strings :weight 'bold :height 1.2)
   (doom-themes-treemacs-file-face :foreground comments)
   )


  ;; --- extra variables --------------------
  ;; ()

  )

;;; doom-everforest-light-theme.el ends here
