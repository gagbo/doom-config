;;; ~/.config/doom/+popups.el -*- lexical-binding: t; -*-

(set-popup-rules!
  '(("^\\*info\\*"
     :slot 2 :side left :width 83 :quit nil)
    ("^\\*\\(?:Wo\\)?Man "
     :vslot -6 :size 0.45 :select t :quit nil :ttl 0)
    ("^\\*ielm\\*$"
     :vslot 2 :size 0.4 :quit nil :ttl nil)
    ;; `help-mode', `helpful-mode'
    ("^\\*[Hh]elp"
     :slot 2 :vslot -8 :size 0.45 :select t)
    ("^\\*Checkdoc Status\\*$"
     :vslot -2 :select ignore :quit t :ttl 0)
    ("^\\*\\(?:[Cc]ompil\\(?:ation\\|e-Log\\)\\|Messages\\)"
     :slot -2 :size 0.45 :side right :autosave t :quit current :ttl nil
     :modeline t)
    ("^\\*rust"
     :slot -2 :size 0.45 :side right :autosave t :quit current :ttl nil
     :modeline t)
    ("^ \\*undo-tree\\*"
     :slot 2 :side left :size 20 :select t :quit t)
    ("^\\*\\(?:doom \\|Pp E\\)"  ; transient buffers (no interaction required)
     :vslot -3 :size +popup-shrink-to-fit :autosave t :select ignore :quit t :ttl 0)
    ("^\\*Backtrace" :vslot 99 :size 0.4 :quit nil)
    ("^\\*\\(?:Proced\\|timer-list\\|Process List\\|Abbrevs\\|Output\\|Occur\\|unsent mail\\)\\*" :ignore t)
    ("^\\*Flycheck errors\\*$"
     :vslot -2 :select t :quit t :ttl 0)))
