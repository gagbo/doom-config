;;; ~/.doom.d/+faces.el -*- lexical-binding: t; -*-

(custom-set-faces!
  `(markdown-code-face
    :background ,(doom-color 'bg-alt))
  `(markdown-markup-face
    :foreground ,(doom-color 'blue)))
