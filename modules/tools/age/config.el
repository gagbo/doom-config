;;; tools/age/config.el -*- lexical-binding: t; -*-

(use-package age
  :config
  (age-file-enable)
  :custom
  (age-program "rage")
  (age-default-identity "~/.ssh/id_ed25519")
  (age-default-recipient '("~/.ssh/id_ed25519.pub")))

(after! auth-source
  (pushnew! auth-sources "~/.authinfo.age"))

(when (modulep! :tools age +pass)
  (use-package passage
    :config
    (fset #'pass (cmd! (passage)))))
