;; -*- no-byte-compile: t; -*-
;;; tools/age/packages.el


(package! age)
(when (modulep! :tools age +pass)
  (package! passage))
