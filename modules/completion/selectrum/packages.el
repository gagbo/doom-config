;; -*- no-byte-compile: t; -*-
;;; completion/selectrum/packages.el

(package! selectrum)
(when (featurep! +prescient)
  (package! selectrum-prescient))
