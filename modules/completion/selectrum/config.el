;;; completion/selectrum/config.el -*- lexical-binding: t; -*-

(use-package! selectrum
  :commands (selectrum-mode))

(use-package! selectrum-prescient
  :when (featurep! +prescient)
  :commands (selectrum-prescient-mode))

(add-hook! '(doom-first-input-hook)
  (selectrum-mode +1)
  (when (featurep! +prescient)
    (selectrum-prescient-mode +1)
    (prescient-persist-mode +1)))

;; Projectile defaults to forcing icomplete instead of completing-read
(after! projectile
  (setq projectile-completion-system 'default))

;; For :input layout bepo
(after! selectrum
  (doom-bepo-rotate-bare-keymap '(selectrum-minibuffer-map) doom-bepo-cr-rotation-style))
