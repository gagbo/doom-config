;;; bindings/gagbo/config.el --- Keybinds I overwrite -*- lexical-binding: t; -*-

(map!
;;;; Window movement
 :n "gsi" #'oi-jump

 (:map evil-treemacs-state-map
  "M-j" #'multi-next-line
  "M-k" #'multi-previous-line)

 (:leader
;;;; Quit
  (:prefix "q"
   :desc "Restart Emacs"                "r" #'doom/restart
   :desc "Restart & restore Emacs"      "R" #'doom/restart-and-restore
   :desc "Save buffers and kill server" "Q" #'save-buffers-kill-emacs)
;;;; Imenu
  (:prefix "o"
   :desc "Imenu list"                   "i" #'gagbo-imenu-toggle-maybe-lsp)
;;;; Toggles
  (:prefix "t"
   :desc "Read-only mode"               "r" #'read-only-mode
   :desc "Ghost (Transparency)" "G" #'gagbo/toggle-transparency)
;;;; Magit
  (:prefix "g"
   :desc "Magit file dispatch"          "d" #'magit-file-dispatch
   :desc "Create or checkout branch"    "b" #'magit-branch-or-checkout))

;;;; Hydra
 (:leader
  (:when (featurep! :ui hydra)
   (:prefix "w"
    :desc "Interactive menu"    "w" #'+hydra/window-nav/body)
   (:prefix ("z" . "zoom")
    :desc "Text"                "t" #'+hydra/text-zoom/body)))

;;;; Isearch
 (:after isearch
  (:map isearch-mode-map
   [return] #'+isearch-exit-start-of-match
   "RET"    #'+isearch-exit-start-of-match
   "C-RET"  #'isearch-exit))

;;;; Ivy
 (:after ivy
  (:map ivy-minibuffer-map
   [return] #'ivy-alt-done
   "RET"    #'ivy-alt-done))

;;;; Outshine
 (:after outshine
  (:map outshine-mode-map
   "TAB" #'outshine-cycle
   [tab] #'outshine-cycle
   [M-up] nil
   [M-left] nil
   [M-right] nil
   [M-down] nil))

;;;; Company
 (:after company
  (:map company-active-map
   [tab] nil
   "TAB" nil))

;;;; Yasnippet
 (:after yasnippet
  (:map yas-keymap
   [tab] #'yas-next-field
   "TAB" #'yas-next-field))

;;;; fzf
 ;; (map! :leader "SPC" #'fzf-projectile)

;;;; Flycheck
 (:after flycheck
  (:map flycheck-mode-map
   "M-n" #'flycheck-next-error
   "M-p" #'flycheck-previous-error))

;;;; org-jira
 (:after org-jira
  (:map org-jira-entry-mode-map
   :localleader
   (:prefix ("j" . "Jira Integration")
    (:prefix ("c" . "Comment")
     :desc "Add comment" "c" #'org-jira-add-comment
     :desc "Update comment" "u" #'org-jira-update-comment)
    (:prefix ("i" . "Issue")
     :desc "Assign issue" "a" #'org-jira-assign-issue
     :desc "Browse issue" "b" #'org-jira-browse-issue
     :desc "Create issue" "c" #'org-jira-create-issue
     :desc "Get issues by fixversion" "f" #'org-jira-get-issues-by-fixversion
     :desc "Get issues" "g" #'org-jira-get-issues
     :desc "Get issue heads" "h" #'org-jira-get-issues-headonly
     :desc "Query issues custom jql" "j" #'org-jira-get-issues-from-custom-jql
     :desc "Copy issue key" "k" #'org-jira-copy-current-issue-key
     :desc "Refresh issue" "r" #'org-jira-refresh-issue
     :desc "Refresh all issues" "R" #'org-jira-refresh-issues-in-buffer
     :desc "Next step issue" "n" #'org-jira-progress-issue-next
     :desc "Update issue" "u" #'org-jira-update-issue
     :desc "Issue workflow" "w" #'org-jira-progress-issue)
    (:prefix ("s" . "Subtask")
     :desc "Create subtask" "c" #'org-jira-create-subtask
     :desc "Get Subtask" "g" #'org-jira-get-subtasks)
    (:prefix ("p" . "Project")
     :desc "Get projects" "g" #'org-jira-get-projects)
    :desc "Send TODO to jira" "t" #'org-jira-todo-to-jira
    :desc "Update logs from clock" "u" #'org-jira-update-worklogs-from-org-clocks)))

;;;; Languages
;;;;; Rust
 (:after rustic
  (:map rustic-mode-map
   :localleader
   (:prefix ("r" . "Rustic")
    :desc "Clippy pretty"     "C" #'rustic-cargo-clippy
    :desc "Popup"             "r" #'rustic-popup
    :desc "Format everything" "f" #'rustic-cargo-fmt
    :desc "Cargo-outdated"    "u" #'rustic-cargo-outdated)))

;;;;; Python
 (:after python
  (:map python-mode-map
   :localleader
   :desc "Blacken buffer" "cb" #'blacken-buffer)))
