;;; private/my-cc/config.el -*- lexical-binding: t; -*-

;;;;; Settings
;; GDB settings
(setq gdb-show-main t
      gdb-many-windows t)

;;;;; Flycheck
(after! (cc-mode flycheck)
  (gagbo--cc-flycheck-setup))

;;;;; Bindings
(map!
 (:after cc-mode
  :map (c-mode-map c++-mode-map)
  (:leader
   :n "=" #'clang-format-region))
 (:after ccls
  :map (c-mode-map c++-mode-map)
  :localleader
  :desc "breakpoint"
  :n "db" #'gagbo/cc-insert-breakpoint))


;;;;; LSP settings
(when (featurep! :tools lsp)
  (setq lsp-clients-clangd-args '("-j=2"
                                  "--background-index"
                                  "--clang-tidy"
                                  "--completion-style=detailed"
                                  "--header-insertion=never")))

;;;;; Packages
(use-package! clang-format
  :commands (clang-format-region))

(use-package! google-c-style
  :after cc-mode
  :config
  (c-add-style "Google" google-c-style))

(after! cc-mode
  (add-to-list 'auto-mode-alist '("\\.ipp\\'" . c++-mode))

  (font-lock-add-keywords
   'c++-mode '(("\\<\\(\\w+::\\)" . font-lock-constant-face)))

  (after! smartparens
    (sp-local-pair 'c++-mode "(" nil :post-handlers '(:rem ("||\n[i]" "RET")))
    (sp-local-pair 'c++-mode "{" nil :post-handlers '(:rem ("| "      "SPC"))))

  (advice-add 'c-electric-colon :after #'+cc-better-electric-colon-a)

  (c-add-style
   "gagbo" '("Google"
            (tab-width      . 8)
            (c-basic-offset . 4)
            (c-offsets-alist
             (inlambda              . 0)
             (inexpr-statement      +cc-collapse-brace-list +)
             (arglist-intro         +cc-collapse-brace-list google-c-lineup-expression-plus-4)
             (arglist-cont          +cc-collapse-brace-list 0)
             (arglist-cont-nonempty +cc-collapse-brace-list ++)
             (arglist-close         +cc-better-arglist-close 0)
             (statement-cont        . ++)
             (template-args-cont    . ++))))

  (setf (alist-get 'c++-mode c-default-style) "gagbo")

  (setq +cc-default-header-file-mode 'c++-mode))
