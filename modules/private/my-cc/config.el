;;; private/my-cc/config.el -*- lexical-binding: t; -*-

;;;;; Settings
;;;;; Flycheck
(when (and (modulep! :checkers syntax)
           (not (modulep! :checkers syntax +flymake)))
  (after! (cc-mode flycheck)
    (gagbo--cc-flycheck-setup)))

;;;;; Bindings
(map!
 (:after cc-mode
  :map (c-mode-map c++-mode-map)
  (:leader
   :n "=" #'clang-format-region)))


;;;;; LSP settings
(when (and (modulep! :tools lsp)
           (not (modulep! :tools lsp +eglot)))
  (setq lsp-clients-clangd-args '("-j=4"
                                  "--background-index"
                                  "--clang-tidy"
                                  "--completion-style=detailed"
                                  "--header-insertion=never")))

;;;;; Packages
(use-package! clang-format
  :commands (clang-format-region))

(use-package! google-c-style
  :after cc-mode
  :config
  (c-add-style "Google" google-c-style))

(after! cc-mode
  (font-lock-add-keywords
   'c++-mode '(("\\<\\(\\w+::\\)" . font-lock-constant-face)))

  (after! smartparens
    (sp-local-pair 'c++-mode "(" nil :post-handlers '(:rem ("||\n[i]" "RET")))
    (sp-local-pair 'c++-mode "{" nil :post-handlers '(:rem ("| "      "SPC"))))

  (advice-add 'c-electric-colon :after #'+cc-better-electric-colon-a)

  (c-add-style
   "gagbo" '("Google"
             (tab-width      . 8)
             (c-basic-offset . 4)
             (c-offsets-alist
              (inlambda              . 0)
              (inexpr-statement      gagbo--cc-collapse-brace-list +)
              (arglist-intro         gagbo--cc-collapse-brace-list google-c-lineup-expression-plus-4)
              (arglist-cont          gagbo--cc-collapse-brace-list 0)
              (arglist-cont-nonempty gagbo--cc-collapse-brace-list ++)
              (arglist-close         gagbo--cc-better-arglist-close 0)
              (statement-cont        . ++)
              (template-args-cont    . ++))))

  (setf (alist-get 'c++-mode c-default-style) "gagbo")

  (setq +cc-default-header-file-mode 'c++-mode))
