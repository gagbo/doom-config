;;; private/my-cc/autoload.el -*- lexical-binding: t; -*-
;; Autoload for personal CC configuration

;;;###autoload
(defun gagbo--cc-flycheck-setup ()
  "Setup Flycheck checkers for CC modes"
  (require 'flycheck-clang-tidy)
  (flycheck-clang-tidy-setup)
  (cond ((featurep! :tools lsp +eglot) (after! eglot (flycheck-add-next-checker 'eglot 'c/c++-clang-tidy)))
        ((featurep! :tools lsp)        (after! lsp (push 'c/c++-clang-tidy flycheck-checkers)))))
