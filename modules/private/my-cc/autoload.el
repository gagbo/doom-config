;;; private/my-cc/autoload.el -*- lexical-binding: t; -*-
;; Autoload for personal CC configuration

;;;###autoload
(defun gagbo--cc-flycheck-setup ()
  "Setup Flycheck checkers for CC modes"
  (require 'flycheck-clang-tidy)
  (flycheck-clang-tidy-setup)
  (cond ((modulep! :tools lsp +eglot) (after! eglot (flycheck-add-next-checker 'eglot 'c/c++-clang-tidy)))
        ((modulep! :tools lsp)        (after! lsp (push 'c/c++-clang-tidy flycheck-checkers)))))

;; Functions coming from https://github.com/flatwhatson/doom.d/blob/d524c02bf026991cf914c23e3e077326c1577c69/autoload.el#L3

;;;###autoload
(defun gagbo--cc-collapse-brace-list (langelem)
  "Collapse extra indentation inside a brace-list."
  (when (or (save-excursion
              (save-match-data
                (beginning-of-line)
                (skip-chars-backward " \t\n" (c-langelem-pos langelem))
                (eq (char-before) ?\{)))
            (save-excursion
              (save-match-data
                (beginning-of-line)
                (skip-chars-forward " \t\n")
                (eq (char-after) ?\}))))
    0))

;;;###autoload
(defun gagbo--cc-better-arglist-close (langelem)
  "Indent arglist-close as though the closing paren was not present."
  ;; TODO indent unless followed by ; or {
  (let ((symbol
         (save-excursion
           (save-match-data
             (beginning-of-line)
             (skip-chars-backward " \t\n" (c-langelem-pos langelem))
             (cond ((eq (char-before) ?\() 'arglist-intro)
                   ((eq (char-before) ?\,) 'arglist-cont-nonempty)
                   ((eq (char-before) ?\;) 'statement)
                   (t                      'arglist-cont))))))
    (when symbol
      (c-calc-offset (cons symbol (cdr langelem))))))
