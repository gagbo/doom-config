;; -*- no-byte-compile: t; -*-
;;; private/my-cc/packages.el

(package! clang-format)
(package! cmake-mode :recipe (:host github :repo "emacsmirror/cmake-mode" :files (:defaults "*")))
(when (modulep! :completion ivy)
  (package! counsel-test))
(when (modulep! :checkers syntax)
  (package! flycheck-clang-tidy))

(package! google-c-style
  :recipe (:host github :repo "google/styleguide" :branch "gh-pages"))
