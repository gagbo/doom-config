;;; lang/fennel/config.el -*- lexical-binding: t; -*-

(use-package! fennel-mode
  :commands fennel-mode)
(add-to-list 'auto-mode-alist '("\\.fnl\\'" . fennel-mode))
