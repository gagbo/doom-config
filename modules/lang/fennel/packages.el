;; -*- no-byte-compile: t; -*-
;;; lang/fennel/packages.el

(package! fennel-mode :recipe (:host gitlab :repo "technomancy/fennel-mode"))
