;;; lang/powershell/config.el -*- lexical-binding: t; -*-

(use-package! powershell
  :defer t
  :mode ("\\.ps1\\'" . powershell-mode))
