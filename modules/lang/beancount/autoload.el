;;; lang/beancount/autoload.el -*- lexical-binding: t; -*-

;;;###autoload
(defun +beancount-format ()
  "Format a file and replace the current buffer with the file formatted.
DATA LOSS WARNING : Save the buffer before calling it if running inplace."
  (interactive)
  (let ((beancount-file (file-relative-name buffer-file-name))
        (old-point (point)))
    (with-temp-buffer
      (if (= 0 (call-process +beancount-format-program nil t nil
                             beancount-file))
          (copy-to-buffer (get-file-buffer beancount-file) (point-min) (point-max))
        (message "Could not format buffer because of error, try running %s %s to see" +beancount-format-program beancount-file)))))
