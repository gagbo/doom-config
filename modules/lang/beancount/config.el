;;; lang/beancount/config.el -*- lexical-binding: t; -*-

(setq load-path (push
                 (concat (file-name-as-directory doom-private-dir) "modules/lang/beancount")
                 load-path))

(defvar +beancount-format-program "bean-format"
  "The name of the formatter for beancount files.
The formatter must take a filename as input (no stdin),
and put formatted output to stdout.")

(use-package! beancount
  :defer t
  :mode ("\\.beancount\\'" . beancount-mode)
  :init
  (setq beancount-mode-map-prefix [(control c)]
        beancount-electric-currency t
        beancount-use-ido nil)
  :config
  (map! :map beancount-mode-map
        [tab] #'beancount-tab-dwim)
  (map! :localleader
        :map beancount-mode-map
        :desc "Insert Account" "'" #'beancount-insert-account
        :desc "Clear Transaction" "C-g" #'beancount-transaction-clear
        :desc "Check" "l" #'beancount-check
        :desc "Format" "f" #'+beancount-format
        :desc "Run bean-query" "q" #'beancount-query
        :desc "Context (Doctor)" "x" #'beancount-context
        :desc "Linked (Doctor)" "k" #'beancount-linked
        :desc "Insert Prices" "p" #'beancount-insert-prices
        :desc "Align to previous" ";" #'beancount-align-to-previous-number
        :desc "Align" ":" #'beancount-align-numbers))
