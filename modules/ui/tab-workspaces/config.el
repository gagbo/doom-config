;;; ui/tab-workspaces/config.el -*- lexical-binding: t; -*-

(defvar doom-workspaces-default-name "Main")

;; TODO: find the correct doom hook
(use-package tabspaces
  :hook (after-init . tabspaces-mode)
  :init (setq tabspaces-keymap-prefix "C-c TAB")
  :commands (tabspaces-switch-or-create-workspace
              tabspaces-open-or-create-project-and-workspace)
  :config (when (modulep! :editor evil)
            (map! :leader
                  :desc "Tab workspaces" "TAB" tabspaces-command-map))
  (define-key tabspaces-command-map (kbd "O") #'tabspaces-project-switch-project-open-file)
  (define-key tabspaces-command-map (kbd "C") 'tabspaces-clear-buffers)
  (define-key tabspaces-command-map (kbd "b") 'tabspaces-switch-to-buffer)
  (define-key tabspaces-command-map (kbd "d") 'tabspaces-close-workspace)
  (define-key tabspaces-command-map (kbd "o") 'tabspaces-open-or-create-project-and-workspace)
  (define-key tabspaces-command-map (kbd "r") 'tabspaces-remove-current-buffer)
  (define-key tabspaces-command-map (kbd "R") 'tabspaces-remove-selected-buffer)
  (define-key tabspaces-command-map (kbd "s") 'tabspaces-switch-or-create-workspace)
  (define-key tabspaces-command-map (kbd "t") 'tabspaces-switch-buffer-and-tab)
  :custom
  (tabspaces-use-filtered-buffers-as-default t)
  (tabspaces-default-tab doom-workspaces-default-name)
  ;; TODO: This variable should be let-bound in multiple wrapper commands around
  ;; removing buffers
  (tabspaces-remove-to-default nil)
  (tabspaces-session t)
  (tabspaces-include-buffers '("*Messages*"))
  (tabspaces-include-buffers '("*scratch*")))

(when (modulep! :completion vertico)
  (with-eval-after-load 'consult
    ;; hide full buffer list (still available with "b" prefix)
    (consult-customize consult--source-buffer :hidden t :default nil)
    ;; set consult-workspace buffer list
    (defvar consult--source-workspace
      (list :name     "Workspace Buffers"
            :narrow   ?w
            :history  'buffer-name-history
            :category 'buffer
            :state    #'consult--buffer-state
            :default  t
            :items    (lambda () (consult--buffer-query
                                  :predicate #'tabspaces--local-buffer-p
                                  :sort 'visibility
                                  :as #'buffer-name)))

      "Set workspace buffer list for consult-buffer."))

  (defun my--consult-tabspaces ()
    "Deactivate isolated buffers when not using tabspaces."
    (require 'consult)
    (cond (tabspaces-mode
           ;; hide full buffer list (still available with "b")
           (consult-customize consult--source-buffer :hidden t :default nil)
           (add-to-list 'consult-buffer-sources 'consult--source-workspace))
          (t
           ;; reset consult-buffer to show all buffers 
           (consult-customize consult--source-buffer :hidden nil :default t)
           (setq consult-buffer-sources (remove #'consult--source-workspace consult-buffer-sources)))))

  (add-hook 'tabspaces-mode-hook #'my--consult-tabspaces))



(when (modulep! :completion ivy)
  (defun tabspaces-ivy-switch-buffer (buffer)
    "Display the local buffer BUFFER in the selected window.
This is the frame/tab-local equivilant to `switch-to-buffer'."
    (interactive
     (list
      (let ((blst (mapcar #'buffer-name (tabspaces-buffer-list))))
        (read-buffer
         "Switch to local buffer: " blst nil
         (lambda (b) (member (if (stringp b) b (car b)) blst))))))
    (ivy-switch-buffer buffer)))


(defun doom--tabspace-setup ()
  "Set up tabspace at startup."
  ;; Add *Messages* and *splash* to Tab \`Home\'
  (tabspaces-mode 1)
  (progn
    (tab-bar-rename-tab doom-workspaces-default-name)
    (when (get-buffer "*Messages*")
      (set-frame-parameter nil
                           'buffer-list
                           (cons (get-buffer "*Messages*")
                                 (frame-parameter nil 'buffer-list))))
    (when (get-buffer "*splash*")
      (set-frame-parameter nil
                           'buffer-list
                           (cons (get-buffer "*splash*")
                                 (frame-parameter nil 'buffer-list))))))

(add-hook 'after-init-hook #'doom--tabspace-setup)

(defun doom-tabspaces-project-save-buffers ()
  "Save buffers for the current project, prompting for a project if nothing is found."
  (interactive)
  (let ((current-project (project-current t)))
    (save-some-buffers t (lambda () (member (current-buffer) (project-buffers current-project))))))

(defun doom--workspaces-remap-project-commands ()
  "Set up project-related commands to use project.el or tabspaces.el functions instead of projectile"
  (map! :leader 
        :desc "Open project in new tab" "pp" #'tabspaces-open-or-create-project-and-workspace
        ;; TODO: Should be a prefix command of SPC-p-p really but later.
        :desc "Open project in current tab" "pP" #'tabspaces-project-switch-project-open-file
        :desc "Kill buffers and workspace" "pk" #'tabspaces-kill-buffers-close-workspace
        :desc "Save project buffers" "ps" #'doom-tabspaces-project-save-buffers
        :desc "Bury/remove current buffer from workspace" "bd" #'tabspaces-remove-current-buffer))

(add-hook 'doom-after-modules-config-hook #'doom--workspaces-remap-project-commands)

;; Advising tab-bar.el to apply emacs commit eef6626b55e59d4a76e8666108cc68a578fac793
;;
;; Without this advice, tabspaces-switch-or-create-workspace cannot create
;; a new workspace

;; (defadvice! doom-tab-workspaces--upstream-tab-bar-switch-to-tab-a (orig-fn name)
;;  "Switch to the tab by NAME.
;; Default values are tab names sorted by recency, so you can use \
;; \\<minibuffer-local-map>\\[next-history-element]
;; to get the name of the most recently visited tab, the second
;; most recent, and so on.
;; When the tab with that NAME doesn't exist, create a new tab
;; and rename it to NAME."
;;  :around #'tab-bar-switch-to-tab
;;  (interactive
;;   (let* ((recent-tabs (mapcar (lambda (tab)
;;                                 (alist-get 'name tab))
;;                               (tab-bar--tabs-recent))))
;;     (list (completing-read (format-prompt "Switch to tab by name"
;;                                           (car recent-tabs))
;;                            recent-tabs nil nil nil nil recent-tabs))))
;;  (let ((tab-index (tab-bar--tab-index-by-name name)))
;;    (if tab-index
;;        (tab-bar-select-tab (1+ tab-index))
;;      (tab-bar-new-tab)
;;      (tab-bar-rename-tab name))))
