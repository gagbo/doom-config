#+TITLE:   ui/tab-workspaces
#+DATE:    july 19, 2022
#+SINCE:   3.0
#+STARTUP: inlineimages nofold

* Table of Contents :TOC_3:noexport:
- [[#description][Description]]
  - [[#maintainers][Maintainers]]
  - [[#module-flags][Module Flags]]
  - [[#plugins][Plugins]]
  - [[#hacks][Hacks]]
- [[#prerequisites][Prerequisites]]
- [[#features][Features]]
- [[#configuration][Configuration]]
- [[#troubleshooting][Troubleshooting]]

* Description

Provide persistent workspaces using Emacs 27+ tab-bar feature

** Maintainers
+ @gagbo (Author)

** Module Flags
This module provides no flags.

** Plugins
+ [[https://github.com/mclear-tools/tabspaces][tabspaces]]

** Hacks
This modules aims to bring persistence over the (intentionally) barebones
tabspaces package.

* Prerequisites
This module has no prerequisites.

* Features
# An in-depth list of features, how to use them, and their dependencies.

* Configuration
# How to configure this module, including common problems and how to address them.

* Troubleshooting
# Common issues and their solution, or places to look for help.
