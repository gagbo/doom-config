;;; ~/.config/doom/autoload/flycheck.el -*- lexical-binding: t; -*-

;;;###autoload
(defun gagbo--go-flycheck-setup ()
  "Setup Flycheck checkers for Golang"
  (flycheck-golangci-lint-setup)
  (cond ((modulep! :tools lsp +eglot) (after! eglot (flycheck-add-next-checker 'eglot 'golangci-lint)))
        ((modulep! :tools lsp) (after! lsp (push 'golangci-lint flycheck-checkers)))))

;;;###autoload
(defun gagbo--python-flycheck-setup ()
  "Setup Flycheck checkers for Python"
  (cond ((modulep! :tools lsp +eglot)
         (after! eglot
           (progn
             (flycheck-add-next-checker 'eglot 'python-pylint)
             (flycheck-add-next-checker 'eglot 'python-mypy))))
        ((modulep! :tools lsp)
         (after! lsp
           (progn
             (push 'python-pylint flycheck-checkers)
             (push 'python-mypy flycheck-checkers))))))
