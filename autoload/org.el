;;; autoload/org.el -*- lexical-binding: t; -*-

(require 'org-element)

;; Source: https://d12frosted.io/posts/2020-06-24-task-management-with-roam-vol2.html
;;;###autoload
(defun gagbo/org-agenda-category (&optional len)
  "Get category of item at point for agenda.

Category is defined by one of the following items:

- CATEGORY property
- TITLE keyword
- TITLE property
- filename without directory and extension

When LEN is a number, resulting string is padded right with
spaces and then truncated with ... on the right if result is
longer than LEN.

Usage example:

  (setq org-agenda-prefix-format
        '((agenda . \" %(vulpea-agenda-category) %?-12t %12s\")))

Refer to `org-agenda-prefix-format' for more information."
  (let* ((file-name (when buffer-file-name
                      (file-name-sans-extension
                       (file-name-nondirectory buffer-file-name))))
         (title (gagbo/org--buffer-title-get))
         (category (org-get-category))
         (result
          (or (if (and
                   title
                   (string-equal category file-name))
                  title
                category)
              "")))
    (require 's)
    (if (numberp len)
        (s-truncate len (s-pad-right len " " result))
      result)))

;; Taken as-is from vulpea code base (`vulpea-buffer-prop-get`)
(defun gagbo/org--buffer-prop-get (name)
  "Get a buffer property called NAME as a string."
  (org-with-point-at 1
    (when (re-search-forward (concat "^#\\+" name ": \\(.*\\)")
                             (point-max) t)
      (let ((value (string-trim
                    (buffer-substring-no-properties
                     (match-beginning 1)
                     (match-end 1)))))
        (unless (string-empty-p value)
          value)))))

;; Taken as-is from vulpea code base (`vulpea-buffer-title-get`)
(defun gagbo/org--buffer-title-get ()
  "Get the TITLE in current buffer."
  (gagbo/org--buffer-prop-get "title"))

(defun gagbo/org--title-as-tag ()
  "Return title of the current note as tag."
  (gagbo/org--title-to-tag (gagbo/org--buffer-title-get)))

(defun gagbo/org--title-to-tag (title)
  "Convert TITLE to tag."
  (require 's)
  (concat "@" (s-replace " " "" title)))

(defun gagbo/org-project-p ()
  "Return non-nil if current buffer has any todo entry.

TODO entries marked as done are ignored, meaning the this
function returns nil if current buffer contains only completed
tasks."
  (org-element-map
      (org-element-parse-buffer 'headline)
      'headline
    (lambda (h)
      (eq (org-element-property :todo-type h)
          'todo))
    nil 'first-match))

(defun gagbo/org-buffer-p ()
  "Return non-nil if the currently visited buffer is a note."
  (and buffer-file-name
       (string-prefix-p
        (expand-file-name (file-name-as-directory org-roam-directory))
        (file-name-directory buffer-file-name))))
