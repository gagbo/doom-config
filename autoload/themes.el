;;; ~/.config/doom/autoload/themes.el -*- lexical-binding: t; -*-

;;;###autoload
(defun gagbo-circadian-theme ()
  "Sets the theme according to the hour in the current time.

  If the hour is (both inclusive) in `gagbo-light-theme-hours' then
  `gagbo-light-theme' is loaded, otherwise `gagbo-dark-theme' is loaded."
  (interactive)
  (let ((current-time (decode-time)))
    (if (gagbo-timestamp-between-p current-time gagbo-light-theme-begin gagbo-light-theme-end)
        (setq now gagbo-light-theme)
      (setq now gagbo-dark-theme))
    (unless (equal now current-theme)
      (setq current-theme now)
      (load-theme now t))))

;;;###autoload
(defun gagbo/toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'background-alpha)))
    (set-frame-parameter
     nil 'background-alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(90 . 80) '(100 . 100)))))

;;;###autoload
(defun gagbo/modus-themes-custom-faces (theme)
  (message "Applying modus customizations to %s" theme)
  (modus-themes-with-colors
    (custom-theme-set-faces theme
     `(rainbow-delimiters-depth-1-face ((,c :foreground ,fg-main)))
     `(rainbow-delimiters-depth-2-face ((,c :foreground ,blue-warmer)))
     `(rainbow-delimiters-depth-3-face ((,c :foreground ,green-warmer)))
     `(rainbow-delimiters-depth-4-face ((,c :foreground ,cyan-warmer)))
     `(rainbow-delimiters-depth-5-face ((,c :foreground ,blue-faint)))
     `(rainbow-delimiters-depth-6-face ((,c :foreground ,green-faint)))
     `(rainbow-delimiters-depth-7-face ((,c :foreground ,cyan-faint)))
     `(rainbow-delimiters-depth-8-face ((,c :foreground ,blue-cooler)))
     `(rainbow-delimiters-depth-9-face ((,c :foreground ,green-cooler)))

     `(flycheck-posframe-face ((,c :foreground ,fg-alt)))
     `(flycheck-posframe-background-face ((,c :background ,bg-active)))
     `(flycheck-posframe-border-face ((,c :foreground ,border)))
     `(flycheck-posframe-info-face ((,c :foreground ,info)))
     `(flycheck-posframe-warning-face ((,c :foreground ,warning)))
     `(flycheck-posframe-error-face ((,c :foreground ,err)))

     `(solaire-default-face ((,c :inherit default :background ,bg-dim :foreground ,fg-dim)))
     `(solaire-line-number-face ((,c :inherit solaire-default-face :foreground ,fg-dim)))
     `(solaire-hl-line-face ((,c :background ,bg-active)))
     `(solaire-org-hide-face ((,c :background ,bg-dim :foreground ,bg-dim))))))

;;;###autoload
(defun gagbo/ef-themes-todo-faces ()
    (ef-themes-with-colors
       (setq hl-todo-keyword-faces
             `(("HOLD" . ,yellow)
               ("TODO" . ,red)
               ("NEXT" . ,blue)
               ("THEM" . ,magenta)
               ("PROG" . ,cyan-warmer)
               ("OKAY" . ,green-warmer)
               ("DONT" . ,yellow-warmer)
               ("FAIL" . ,red-warmer)
               ("BUG" . ,red-warmer)
               ("DONE" . ,green)
               ("NOTE" . ,blue-warmer)
               ("KLUDGE" . ,cyan)
               ("HACK" . ,cyan)
               ("TEMP" . ,red)
               ("FIXME" . ,red-warmer)
               ("XXX+" . ,red-warmer)
               ("REVIEW" . ,red)
               ("DEPRECATED" . ,yellow)))))

;;;###autoload
(defun gagbo/ef-themes-custom-faces (theme)
  (ef-themes-with-colors
     (custom-theme-set-faces theme
      `(font-lock-comment-face ((,c :background ,bg-info :extend t)))
      `(font-lock-keyword-face ((,c :weight normal :foreground ,fg-main)))
      `(font-lock-variable-name-face ((,c :weight normal :foreground ,fg-main))))))
